import { HOST } from "./constants";
import axios from "axios";

const HistoryTestService = {
    getList: async (params, accessToken) => {
        return await axios.get(`${HOST}/historyTests`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }, params
        })
    },
    getLog: async (params, accessToken) => {
        return await axios.get(`${HOST}/historyTests/getlog`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }, params
        })
    },
    getDetail: async (accessToken) => {
        return await axios.get(`${HOST}/historyTests`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
    },
    // getDetail: async (params) => {
    //     return await axios.get(`${HOST}/historyTests/${params}`)
    // },
    create: async (body, accessToken) => {
        return await axios.post(`${HOST}/historyTests`, body, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            },
        })
    },
    update: async (userId, body) => {
        return await axios.patch(`${HOST}/historyTests/${userId}`, body,)
    },
    detele: async (roleId) => {
        return await axios.delete(`${HOST}/historyTests/${roleId}`)
    },
}

export default HistoryTestService
