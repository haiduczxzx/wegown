import { HOST } from "./constants";
import axios from "axios";

const userService = {
    // const headers = {
    //     'Authorization': `Bearer ${accessToken}`,
    // };
    // const res = await userService.getListEmployee({ headers });

    getListEmployee: async (params, accessToken) => {
        return await axios.get(`${HOST}/user`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }, params
        })
    },
    getEmployeeInfo: async (params, accessToken) => {
        return await axios.get(`${HOST}/user/${params}`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
    },
    createEmployee: async (body) => {
        return await axios.post(`${HOST}/user/register`, body)
    },
    updateEmployee: async (userId, body) => {
        return await axios.patch(`${HOST}/user/${userId}`, body)
    },
    changePassword: async (body) => {
        return await axios.put(`${HOST}/user/changePassword`, body)
    },
}

export default userService
