import { HOST } from "./constants";
import axios from "axios";

const HistoryService = {
  getList: async (params, accessToken) => {
    return await axios.get(`${HOST}/histories`, {
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }, params
    })
  },
  getDetail: async (params) => {
    return await axios.get(`${HOST}/histories/${params}`)
  },
  create: async (body, accessToken) => {
    return await axios.post(`${HOST}/histories`, body, {
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    })
  },
  update: async (userId, body) => {
    return await axios.patch(`${HOST}/histories/${userId}`, body)
  },
  detele: async (roleId) => {
    return await axios.delete(`${HOST}/histories/${roleId}`)
  },
}

export default HistoryService
