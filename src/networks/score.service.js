import { HOST } from "./constants";
import axios from "axios";

const ScoreService = {
    getList: async (params, accessToken) => {
        return await axios.get(`${HOST}/scores`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }, params
        })
    },
    getDetail: async (params) => {
        return await axios.get(`${HOST}/scores/${params}`)
    },
    getDetailByUserId: async (params, accessToken) => {
        return await axios.get(`${HOST}/scores/findByUserId?id=${params}`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            },
        })
    },
    create: async (body) => {
        return await axios.post(`${HOST}/scores`, body)
    },

    update: async (userId, body) => {
        return await axios.patch(`${HOST}/scores/${userId}`, body)
    },
    detele: async (roleId) => {
        return await axios.delete(`${HOST}/scores/${roleId}`)
    },
}

export default ScoreService
