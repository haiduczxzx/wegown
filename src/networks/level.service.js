
import { HOST } from "./constants";
import axios from "axios";

const LevelService = {
    getList: async (params, accessToken) => {
        return await axios.get(`${HOST}/level`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }, params
        })
    },
    getDetail: async (params) => {
        return await axios.get(`${HOST}/level/${params}`)
    },
    getDetailByUserId: async (params) => {
        return await axios.get(`${HOST}/level/findByUserId?id=${params}`)
    },
    create: async (body) => {
        return await axios.post(`${HOST}/level`, body)
    },

    update: async (userId, body) => {
        return await axios.patch(`${HOST}/level/${userId}`, body)
    },
    detele: async (roleId) => {
        return await axios.delete(`${HOST}/level/${roleId}`)
    },
}

export default LevelService
