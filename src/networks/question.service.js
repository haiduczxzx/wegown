import { HOST } from "./constants";
import axios from "axios";

const QuestionService = {
    getList: async (params) => {
        return await axios.get(`${HOST}/question`, { params })
    },
    getDetail: async (params) => {
        return await axios.get(`${HOST}/question/${params}`)
    },
    create: async (body) => {
        return await axios.post(`${HOST}/question`, body)
    },

    update: async (userId, body) => {
        return await axios.patch(`${HOST}/question/${userId}`, body)
    },
    detele: async (roleId) => {
        return await axios.delete(`${HOST}/lesquestionson/${roleId}`)
    },
}

export default QuestionService
