import { HOST } from "./constants";
import axios from "axios";

// login
export const login = async (body) => {
    var results = await axios.post(`${HOST}/auth/login`, body);
    return results
}
// register
export const register = async (body) => {
    return await axios.post(`${HOST}/user/register`, body)
}
export const getUser = async (token) => {
    return await axios.get(`${HOST}/auth/profile`,
        {
            headers: {
                "Content-Type": 'application/json',
                Authorization: 'Bearer ' + token
            }
        }
    )
}
// home
const roleService = {
    getList: async (params) => {
        return await axios.get(`${HOST}/role`, { params })
    },
    getDetailRole: async (params) => {
        return await axios.get(`${HOST}/role/${params}`)
    },
    getRoleForUser: async (params, accessToken) => {
        return await axios.get(`${HOST}/role/getRoleForUser?id=${params}`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            },
        })
    },
    create: async (body) => {
        return await axios.post(`${HOST}/role`, body)
    },
    update: async (userId, body) => {
        return await axios.patch(`${HOST}/role/${userId}`, body)
    },
    deteleRole: async (roleId) => {
        return await axios.delete(`${HOST}/role/${roleId}`)
    },
}
export default roleService