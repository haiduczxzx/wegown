import { HOST } from "./constants";
import axios from "axios";

const ProgramService = {
  getList: async (accessToken) => {
    return await axios.get(`${HOST}/program?isTrending=true`, {
      headers: {
        "Authorization": `Bearer ${accessToken}`
      },
    })
  },
  getDetail: async (params, accessToken) => {
    return await axios.get(`${HOST}/program/findOneProgramByUser?id=${params}`, {
      headers: {
        "Authorization": `Bearer ${accessToken}`
      },
    }
    )
  },
  getOneProgram: async (params, accessToken) => {
    return await axios.get(`${HOST}/program/${params}`, {
      headers: {
        "Authorization": `Bearer ${accessToken}`
      },
    }
    )
  },
  create: async (body) => {
    return await axios.post(`${HOST}/program`, body)
  },
  update: async (userId, body) => {
    return await axios.patch(`${HOST}/program/${userId}`, body)
  },
  detele: async (roleId) => {
    return await axios.delete(`${HOST}/program/${roleId}`)
  },
}

export default ProgramService
