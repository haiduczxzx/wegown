'use client'
import React, { useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useRouter } from 'next/navigation';
import { getUser } from '../../networks/role.service'
import { HOST } from "@/networks/constants";
import LevelService from '../../networks/level.service';
import ScoreService from '../../networks/score.service';
import userService from '../../networks/use.service';

export default function Page() {
    const router = useRouter();
    const [userInfo, setUserInfo] = useState()
    const [score, setScore] = useState()
    const [name, setName] = useState("")
    const [getList, setGetList] = useState()
    const [listData, setListData] = useState()
    const [dataInfor, setDataInfor] = useState()

    useEffect(() => {
        getListUsers()
    }, [])

    // useEffect(() => {
    //     // getUserInfo()
    //     getDefaultLevel()
    // }, [])

    // useEffect(() => {
    //     if (!userInfo) return
    //     getScore()
    // }, [userInfo])

    // const getUserInfo = async () => {
    //     const token = localStorage.getItem("token")
    //     const user = await getUser(token)
    //     if (user) {
    //         setUserInfo(user?.data?.data)
    //         // console.log("user", user);
    //     }
    // }

    // const getScore = async () => {
    //     // if (!userInfo) return
    //     try {
    //         const accessToken = localStorage.getItem('token')
    //         const res = await ScoreService.getList({}, accessToken)
    //         const data = res.data
    //         console.log("data getScore ---> ", data)
    //         if (data) {
    //             setScore(data.data.score)
    //             // setLevel(data.data.levelId)
    //         }
    //     } catch (error) {
    //         console.error("error", error)
    //     }
    // }

    // const getDefaultLevel = async () => {
    //     try {
    //         const accessToken = localStorage.getItem('token')
    //         const res = await LevelService.getList({ pageSize: 10000 }, accessToken)
    //         const data = res.data
    //         console.log('data', data);
    //         if (data) {
    //             const firstLevel = data.data.find((item) => item.minScore == 0)
    //             // setLevel(firstLevel)
    //             // console.log('firstLevel', firstLevel);
    //             if (firstLevel) {
    //                 setName(firstLevel?.title)
    //                 // console.log("firstLevel?.title", firstLevel?.title);
    //             }
    //         }
    //     } catch (error) {
    //         console.error('get rank', error)
    //     }
    // }

    // const getListUser = async () => {
    //     try {
    //         const accessToken = localStorage.getItem('token')
    //         // console.log(accessToken);
    //         const list = await userService.getListEmployee({}, accessToken)
    //         // console.log(list);
    //         if (list) {
    //             setGetList(list?.data?.data)
    //             // console.log(list, "list");
    //         }
    // } catch (error) {
    //         console.log(error);
    //     }
    // }

    const getListUsers = async () => {
        try {
            const token = localStorage.getItem("token")
            // console.log(token);
            const user = await getUser(token)
            // console.log("user", user);
            // if (!user) return
            const response = await userService.getEmployeeInfo(user?.data?.data?._id, token)
            // console.log(response);
            const data = response.data
            // console.log(data, "data")
            setUserInfo(data)
            if (data?.data.roleId.typeManageHistory == 'all-history') {
                // quan ly toan bo nguoi dung
                const listUsers = await userService.getListEmployee({}, token)
                parseListUser(listUsers.data)
            } else {
                parseListUser(data.childIds)
            }
            // Alert.alert("Xin chào " + data.name)
        } catch (error) {
            console.log("Không thể lấy thông tin", error)
        }
    }

    const parseListUser = async (listUser) => {
        // let listResultUser = listUser
        const { code, data, status, message, totalPage, totalRecord } = listUser;
        // console.log(listUser, "a");
        const promiseGetScore = []
        const token = localStorage.getItem("token")
        // console.log("listUser", listUser);
        data.forEach((user) => {
            // console.log("user", user)
            promiseGetScore.push(ScoreService.getDetailByUserId(user._id, token))
            // console.log("promiseGetScore", promiseGetScore);
        })
        const result = await Promise.all(promiseGetScore)
        // console.log("result", result)

        result.forEach(resultItem => {
            // console.log("resultItem", resultItem)
            const scoreItem = resultItem.data
            // console.log("resultItem.data", scoreItem);
            if (scoreItem && scoreItem?.data?.userId && scoreItem?.data?.levelId) {
                const index = data.findIndex((item) => item?._id == scoreItem.data.userId._id)
                const itemU = data[index]
                // console.log("index", itemU);
                if (itemU) {
                    itemU['levelId'] = scoreItem?.data.levelId
                    itemU['score'] = scoreItem?.data.score
                }
                data[index] = { ...itemU }
                // console.log("listResultUser", itemU);
            }
        })
        // setDataInfor(result)
        const resRank = await ScoreService.getList({ pageSize: 10000 }, token)
        const dataRank = resRank.data
        // console.log("dataRank", dataRank);
        if (dataRank) {
            data.forEach(user => {
                const position = dataRank?.data.findIndex((item) => item.userId._id == user._id)
                if (position && position != -1) {
                    user['position'] = position + 1
                } else {
                    user['position'] = 1
                }
            })
        }
        // console.log("listResultUserId", data)
        setListData({ ...listUser, data })
    }

    const handleClick = () => {
        router.push('/manage')
    }
    return (
        <div className='component-list-user'>
            <div className='gr-list-user'>
                <ArrowBackIosIcon onClick={handleClick} />
                <div className='title-list-user'>Danh sách người dùng</div>
            </div>
            {listData?.data?.map((x, key) => (
                <div className='list-user-name' key={key}>
                    {x?.photo ?
                        <img className='img-list-user' src={`${HOST}/${x?.photo}`} alt="#" />
                        : <img className='img-list-user' src="./df.png" alt="#" />
                    }
                    <div className='gr-point'>
                        <div className='name-user'>{x?.name}</div>
                        <div className='level-list'>
                            <div className='rank-list-user'>Điểm: {x?.score}</div>
                            <div className='rank-list-user'>Xếp hạng: {x.position}</div>
                            <div className='rank-list-user1'>Cấp: {x?.levelId?.title}</div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}
