'use client'
import "./style.css"
import React from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import Image from 'next/image';

function Page() {
    return (
        <div className="component">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Thông báo</div>
                <Image
                    className='img'
                    src="/check.png"
                    width={30}
                    height={30}
                    alt="teenUp"
                />
            </div>
            <div className="content">
                <div className="text">Nội dung thông báo</div>
                <div className="time">08:40 - 15/09/2023</div>
            </div>
            <div className="content">
                <div className="text">Nội dung thông báo</div>
                <div className="time">08:40 - 15/09/2023</div>
            </div>
            <div className="content">
                <div className="text">Nội dung thông báo</div>
                <div className="time">08:40 - 15/09/2023</div>
            </div>
            <div className="content-unread">
                <div className="text">Nội dung thông báo</div>
                <div className="time">08:40 - 15/09/2023</div>
            </div>
            <div className="content-unread">
                <div className="text">Nội dung thông báo</div>
                <div className="time">08:40 - 15/09/2023</div>
            </div>
            <div className="content-unread">
                <div className="text">Nội dung thông báo</div>
                <div className="time">08:40 - 15/09/2023</div>
            </div>
        </div>
    )
}
export default Page