'use client'
import React, { useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useRouter } from 'next/navigation';
import { getUser } from '../../networks/role.service'
// import HistoryTestService from '../../networks/history-test.service';
import QuestionService from '../../networks/question.service'
import ScoreService from '../../networks/score.service'
import { useSearchParams } from "next/navigation"
import axios from "axios";
import { HOST } from '@/networks/constants';

export default function Page() {

    const searchParams = useSearchParams()
    const id = searchParams.get('id')
    const historyId = searchParams.get('historyId')
    const router = useRouter()

    const [userInfo, setUserInfo] = useState()
    const [data, setData] = useState()
    const [itemSelected, setItemSelected] = useState(null)
    const [listQuestion, setListQuestion] = useState()
    // const [params, setParams] = useState({})

    useEffect(() => {
        getUserInfo()
    }, [])

    // useEffect(() => {
    //     if (!data) return
    //     // navigation.setOptions({ title: data?.exerciseId?.title })
    //     console.log("data", data)
    //     setListQuestion(data?.listQuestionIds)
    // }, [data])

    // useEffect(() => {
    //     getDetailHistoryTest(historyId)
    // }, [historyId])

    // useEffect(() => {
    //     makeScoreForItem()
    // }, [])

    useEffect(() => {
        var token = localStorage.getItem("token")
        getDetail(token).then((v) => {
            console.log('get-detail', v);
            setData(v?.data)
        })
    }, [])

    const getDetail = async (accessToken) => {
        return await axios.get(`${HOST}/historyTests/${id}`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
    }

    // const getDetailHistoryTest = async (id) => {
    //     const token = localStorage.getItem("token")
    //     // const user = await getUser(token)
    //     const res = await HistoryTestService.getDetail(token, id)
    //     if (res) {
    //         setData(res?.data)
    //         console.log("res", res?.data);
    //     }
    // }

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    // const makeScoreForItem = async (score, note) => {
    //     try {
    //         if (!itemSelected && !userInfo) return
    //         const resScoreItem = await ScoreService.create({
    //             userId: data.userId._id,
    //             score: score
    //         })
    //         console.log(resScoreItem, "1");
    //         const scoreData = resScoreItem.data
    //         console.log(scoreData, "2");
    //         if (!scoreData) return
    //         const res = await QuestionService.update(itemSelected._id, {
    //             // processing: processing,
    //             // historyId: historyId,
    //             // exerciseId: exerciseId,
    //             // lessonId: lessonId,
    //             userId: data.userId._id,
    //             score: score,
    //             note: note,
    //             makeScoreBy: userInfo._id,
    //             scoreId: scoreData._id,
    //         })
    //         console.log(res, "3");
    //         const resHistoryTest = await HistoryTestService.update(idHistoryTest, {
    //             appendScoreId: scoreData._id
    //         })
    //         console.log(resHistoryTest, "4");
    //         getDetailHistoryTest(idHistoryTest)
    //     } catch (error) {
    //         console.error(error)
    //     }
    // }

    // const makeScoreForItem = async (score) => {
    //     const token = localStorage.getItem("token")
    //     const user = await getUser(token)
    //     console.log(user);
    //     // const score = 100;
    //     const resScoreItem = await ScoreService.create({
    //         userId: user?.userId?._id,
    //         score: score
    //     })
    //     console.log(resScoreItem, "1");
    // }

    // makeScoreForItem();

    const handleClick = () => {
        router.push('/point-management')
    }

    return (
        <div className='component-exercise'>
            <div className='gr-list-user'>
                <ArrowBackIosIcon onClick={handleClick} />
                <div className='title-list-user'>Tên bài học</div>
            </div>
            <div className='gr-infor-exercise'>
                <div className='infor-exercise'>Thông tin chung</div>
                <div className='people-action'>Người thực hiện : {data?.data[0]?.userId?.name}</div>
                <div className='time-exercise'>Thời gian nộp bài : {data?.data?.createdAt}</div>
            </div>
            <div className='gr-to-test'>
                <div className='infor-exercise'>Trắc nghiệm</div>
                <div className='point-exercise'>10/10</div>
            </div>
            <div className='essay'>
                <div className='infor-exercise'>Tự luận</div>
                <button className='btn-exercise'>+ Thêm điểm</button>
            </div>
            <div>Nội dung câu hỏi tự luận?</div>
            <input type="text" placeholder='Nội dung câu trả lời' className='input-exercise' />
        </div>
    )
}