import { useDispatch } from 'react-redux'
import React, { useEffect } from "react";
import { setAuthUserInfo } from '@/redux/auth/authSlice';
import { getUser } from '@/networks/role.service';

const AuthProvider = ({ children }) => {
  const dispatch = useDispatch()
  useEffect(() => {
    var token = localStorage.getItem('token')
    if (token) {
      getUser(token).then((v) => {
        dispatch(setAuthUserInfo(v.data?.data))
      }).catch((_) => {
        dispatch(setAuthUserInfo({}))
      })
    } else {
      dispatch(setAuthUserInfo({}))
    }
  }, [])

  return (
    <>
      {children}
    </>
  )
}

export default AuthProvider
