"use client"
import { useState } from 'react'
import { redirect } from 'next/navigation';
import React from 'react'
import { useEffect } from 'react'
import toast from 'react-hot-toast';
import { getUser } from '@/networks/role.service'

function Protected({ children }) {
    const [loading, setLoading] = useState(true)


    useEffect(() => {
        try {
            var token = localStorage.getItem('token')
            if (!token) {
                setLoading(false)
                toast('Bạn chưa đăng nhập')
                redirect("/login")
            } else {
                getUser(token).then((user) => {
                    setLoading(false)
                    // console.log('user', user);
                    if (!user?.data?.data?._id) {
                        toast('Phiên đăng nhập hết hạn')
                        redirect("/login")
                    }
                })
            }

        } catch (error) {
            setLoading(false)
            toast(`Lỗi: ${error}`)
            redirect("/login")
        }



    }, []);

    return (loading ? <>Loading...</> :
        (<>{children}</>)
    )
}

export default Protected