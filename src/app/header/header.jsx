"use client"
import React, { useState, useEffect } from 'react'
import { Link } from '@mui/material';
import { HOST } from '@/networks/constants';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { getUser } from '@/networks/role.service'

const paths = ["/home", "/wiki", "/information"]

const Page = ({ pathname }) => {

  const [userInfo, setUserInfo] = useState()
  const [hidden, setHidden] = useState(true);
  useEffect(() => {
    getUserInfo()
    //const pathname = window?.location?.pathname
    var index = paths.indexOf(pathname);
    if (index >= 0) {
      setHidden(false);
    } else {
      setHidden(true);
    }
  }, [pathname])

  const getUserInfo = async () => {
    const token = localStorage.getItem("token")
    const user = await getUser(token)
    if (user) {
      setUserInfo(user?.data?.data)
    }
  }


  return (
    hidden ? null : <div className="mx-auto">
      <div className="header">
        <div className="gr">
          {userInfo && (<img
            className='avt'
            src={`${HOST}/${userInfo?.photo}`}
            alt="#"
            width={40}
            height={40}
          />)}
          <div className="intro">Xin chào {userInfo?.name}</div>
        </div>
        <Link className='logout' href="/notification" underline="always">
          <NotificationsIcon className="notify" />
        </Link>
      </div>
    </div>
  )
}

export default Page
