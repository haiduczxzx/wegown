"use client"
import dynamic from 'next/dynamic';
import React, { useState } from 'react'
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import {
    MainContainer,
    ChatContainer,
    MessageList,
    Message,
    MessageInput,
    MessageSeparator,
    ConversationHeader,
    Avatar,
    InfoButton,
    TypingIndicator
} from "@chatscope/chat-ui-kit-react";
import { useRouter } from 'next/navigation';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ReplayIcon from '@mui/icons-material/Replay';
import dayjs from 'dayjs';
//key của dũng
// const API_KEY = "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";
//key của kh
const API_KEY = "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";
export default function Page(props) {

    const router = useRouter()
    const [messages, setMessages] = useState([]);
    const [pendingMsg, setPendingMsg] = useState('');
    const [typing, setTyping] = useState(false);

    const handleClick = () => {
        router.push('/chat-header')
    }

    const handleReload = () => {
        window.location.reload();
    }

    async function handleSend() {
        console.log("click send", pendingMsg)
        setPendingMsg('');
        if (pendingMsg) {
            //push to message List
            const newMessage = {
                sender: 'user',
                sendTime: dayjs(new Date()).format("HH:mm"),
                direction: 'outgoing',
                message: pendingMsg,
                position: 'single'
            }
            setMessages((prevMessages) => [...prevMessages, newMessage]);
            setTyping(true);

            //process gpt response
            try {
                const response = await processMessageToChatGPT([...messages, newMessage]);

                const content = response.choices[0]?.message?.content;
                if (content) {
                    const chatGPTResponse = {
                        message: content,
                        sender: "ChatGPT",
                        sendTime: dayjs(new Date()).format("HH:mm"),
                    };
                    setMessages((prevMessages) => [...prevMessages, chatGPTResponse]);
                }
            } catch (error) {
                console.error("Error processing message:", error);
            } finally {
                setTyping(false);
            }
        }
    }
    async function processMessageToChatGPT(chatMessages) {
        const apiMessages = chatMessages.map((messageObject) => {
            const role = messageObject.sender === "ChatGPT" ? "assistant" : "user";
            return { role, content: messageObject.message };
        });

        const apiRequestBody = {
            "model": "gpt-3.5-turbo",
            "messages": [
                { role: "system", content: "I'm a Student using ChatGPT for learning" },
                ...apiMessages,
            ],
        };

        const response = await fetch("https://api.openai.com/v1/chat/completions", {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + API_KEY,
                "Content-Type": "application/json",
            },
            body: JSON.stringify(apiRequestBody),
        });

        return response.json();
    }
    return (
        <>
            <div className='form-text-chat' style={{ position: "relative" }}>
                <div className='gpt-chat-header'>
                    <div className='gr-gpt'>
                        <ArrowBackIosIcon onClick={handleClick} />
                        <div className='title-chat-gpt'>Trợ lí học tập</div>
                    </div>
                    <ReplayIcon onClick={handleReload} />
                </div>
                <MainContainer className='gpt-main-container'>
                    <ChatContainer>
                        {/* <ConversationHeader> */}
                        {/* <Avatar src={emilyIco} name="Emily" /> */}
                        {/* <ConversationHeader.Content userName="Assistant" info="Active 10 mins ago" /> */}
                        {/* <ConversationHeader.Actions> */}
                        {/* <InfoButton onClick={() => { console.log("clicking voicecall") }} /> */}
                        {/* </ConversationHeader.Actions> */}
                        {/* </ConversationHeader> */}
                        <MessageList>
                            <MessageSeparator content={new Date().toLocaleDateString()} />
                            <Message
                                model={{
                                    message: "Xin chào, Tôi có thể giúp gì cho bạn?",
                                    sentTime: "just now",
                                    sender: "ChatGPT",
                                    direction: "incoming",
                                    position: "single"
                                }}
                                className='bg-white'
                            >
                                {/* <Avatar src={'/WE.png'} name={"ChatGPT"} /> */}
                            </Message>

                            {
                                messages.map((m, key) => (<Message
                                    key={key}
                                    model={m}
                                >
                                </Message>))
                            }
                            {
                                typing && <TypingIndicator content="Gpt is answering" />
                            }
                        </MessageList>

                        <MessageInput placeholder="Type your question" onChange={e => { setPendingMsg(e) }} onSend={handleSend} attachButton={null} />
                    </ChatContainer>
                </MainContainer>
            </div>
        </>

    )
}