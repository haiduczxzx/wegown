"use client"
import React, { useState, useEffect } from 'react'
// import { useRouter } from 'next/router'
// import { Avatar, Stack, Link, Typography, Tabs, Tab } from '@mui/material';
import Image from 'next/image'
const icons = ['/icons/Home_unchecked.svg', '/icons/chat_unchecked.svg', '/icons/manage_unchecked.svg', '/icons/profile_unchecked.svg', '/icons/manage_unchecked.svg']
const iconActives = ['/icons/Home.svg', '/icons/chat.svg', '/icons/manage.svg', '/icons/profile.svg', '/icons/manage.svg']
const paths = ["/home", "/chat-header", "/wiki", "/information", "/manage"]

function Footer({ pathname }) {
  const [value, setValue] = useState(0);
  const [hidden, setHidden] = useState(true);
  useEffect(() => {
    // let pathname = window?.location?.pathname
    // console.log('pathame ', pathname);
    if (pathname == '/')
      pathname = '/home'
    //check index
    var index = paths.indexOf(pathname);
    if (index >= 0) {
      setValue(index);
      setHidden(false);
    } else {
      setHidden(true);
    }
  }, [pathname])


  return (
    hidden ? null : <div className="footer-item fixed bottom-0 w-full flex bg-white pt-3" >
      <a className="flex flex-1 flex-col items-center" href={paths[0]}>
        <div className='flex flex-col'>
          <Image
            className='img_footer-item'
            src={value == 0 ? iconActives[0] : icons[0]}
            width={0}
            style={{ height: 24, width: "auto" }}
            height={0}
            alt="teenUp"
          />
          <span className={`footer-label ${value == 0 && 'txt-active-label'}`}>Trang chủ</span>
        </div>
      </a>
      <a className=" flex flex-1 flex-col items-center " href={paths[1]}>
        <div className='flex flex-col'>
          <Image
            className='img_footer-item'
            src={value == 1 ? iconActives[1] : icons[1]}
            width={0}
            style={{ height: 24, width: "auto" }}
            height={0}
            alt="teenUp"
          />
          <span className={`footer-label ${value == 1 && 'txt-active-label'}`}>Tin nhắn</span>
        </div>
      </a>
      <a className=" flex flex-1 flex-col items-center" href={paths[2]}>
        <div className='flex flex-col'>
          <Image
            className='img_footer-item'
            src={value == 2 ? iconActives[2] : icons[2]}
            width={0}
            style={{ height: 24, width: "auto" }}
            height={0}
            alt="teenUp"
          />
          <span className={`footer-label ${value == 2 && 'txt-active-label'}`}>Wiki</span>
        </div>
      </a>
      <a className=" flex flex-1 flex-col items-center" href={paths[4]}>
        <div className='flex flex-col'>
          <Image
            className='img_footer-item'
            src={value == 4 ? iconActives[4] : icons[4]}
            width={0}
            style={{ height: 24, width: "auto" }}
            height={0}
            alt="teenUp"
          />
          <span className={`footer-label ${value == 4 && 'txt-active-label'}`}>Quản lý</span>
        </div>
      </a>
      <a className=" flex flex-1 flex-col items-center" href={paths[3]}>
        <div className='flex flex-col'>
          <Image
            className='img_footer-item'
            src={value == 3 ? iconActives[3] : icons[3]}
            width={0}
            style={{ height: 24, width: "auto" }}
            height={0}
            alt="teenUp"
          />
          <span className={`footer-label ${value == 3 && 'txt-active-label'}`}>Cá nhân</span>
        </div>
      </a>

    </div>
  )
}

export default Footer
