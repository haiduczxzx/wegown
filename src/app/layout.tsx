
import StyledComponentsRegistry from '@/antd/AntdRegistry'
import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { Toaster } from 'react-hot-toast';
import Wrapper from './wrapper'
import './style.css'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Wegrow',
  description: 'Wegrow giấc mơ trẻ em toàn cầu',
  viewport: 'width=device-width, initial-scale=1, maximum-scale=1'
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">

      <body suppressHydrationWarning={true} className={inter.className}>
        <Wrapper>
          {children}
        </Wrapper>
        <Toaster />

      </body>
    </html>
  )
}
