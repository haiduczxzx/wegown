'use client'
import React from 'react'
import {
    Link,
    Input
} from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import "./style.css"

export default function Page() {
    return (
        <div className='container mx-auto'>
            <div className='title'>
                <Link className='logout' href="/information" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Thay đổi mật khẩu</div>
            </div>
            <div className='form'>
                <Input className='custom_input'
                    id="outlined-read-only-input"
                    placeholder="Tên đăng nhập"
                    disabled
                />
                <Input className='custom_input'
                    id="outlined-read-only-input"
                    placeholder="Mật khẩu"
                />
                <Input className='custom_input'
                    id="outlined-read-only-input"
                    placeholder="Nhập lại mật khẩu"
                />
            </div>
        </div>
    )
}
