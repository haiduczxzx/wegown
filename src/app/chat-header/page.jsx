'use client'
import React, { useEffect, useState } from 'react'
import { getUser } from '@/networks/role.service'
import { HOST } from '@/networks/constants'

export default function Page() {
    const [userInfo, setUserInfo] = useState()

    useEffect(() => {
        getUserInfo()
    }, [])

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    return (
        <div className='chat-web'>
            <div className='user-chat-web'>
                <img src={`${HOST}/${userInfo?.photo}`} alt="" width={50} height={50} className='avt-chat-web' />
                <div className='name-avt-chat'>{userInfo?.name}</div>
            </div>
            <a href='/chat-gpt' className='gr-chat-gpt'>
                <img src="/gpt.png" alt="" width={70} height={50} />
                <div className='gr-conten-chat'>
                    <div className='great-ting'>Chat với Trợ lý học tập AI</div>
                    <div className='introducation'>Xin chào! Mình là trợ lý AI của bạn. Mình ở đây để giải đáp mọi câu hỏi của bạn. Hôm nay bạn cần mình giúp gì không? ^^</div>
                </div>
            </a>
            <a href='/chat-web' className='gr-chat-gpt'>
                <img src="/mess.png" alt="" width={70} height={50} />
                <div className='gr-conten-chat'>
                    <div className='great-ting'>Chat với cố vấn học tập</div>
                    <div className='introducation'>Mọi thắc mắc về phần mềm hoặc chương trình, thứ thách sẽ được các thầy cô trực tiếp giải đáp. Đừng ngại đặt câu hỏi cho thầy cô nhé!</div>
                </div>
            </a>
        </div>
    )
}
