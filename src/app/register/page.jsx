'use client'
import "./style.css"
import * as React from 'react';
import {
    TextField,
    Link,
    OutlinedInput,
    InputAdornment,
    InputLabel,
    IconButton,
    Input,
    FormControl,
    styled,
    Dialog,
    DialogTitle,
    DialogContent,
    Typography,
    DialogActions,
} from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Button } from 'antd';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import CloseIcon from '@mui/icons-material/Close';
import Image from 'next/image';
import axios from 'axios';
import { register } from '../../networks/role.service'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

function Page() {
    // Phần này xử lí phần password
    const [password, setPassword] = React.useState("")
    const [showPassword, setShowPassword] = React.useState(false);
    const [rePassword, setRePassword] = React.useState("");
    const handleClickShowPassword = () => setShowPassword((show) => !show);
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    // Phần này xử lí bật popup đăng kí thành công
    const [open, setOpen] = React.useState(false);
    // API
    const [name, setName] = React.useState("");
    const [username, setUsername] = React.useState("");
    const [phone, setPhone] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [nameValidate, setNameValidate] = React.useState();
    const [userNameValidate, setUserNameValidate] = React.useState();
    const [passwordValidate, setPasswordValidate] = React.useState("");
    const [phoneValidate, setPhoneValidate] = React.useState("");
    const [emailValidate, setEmailValidate] = React.useState("");
    const [rePasswordValidate, setRePasswordValidate] = React.useState();
    const [loading, setLoading] = React.useState(false)

    const validate = () => {
        if (nameValidate && userNameValidate && passwordValidate && phoneValidate && emailValidate && rePasswordValidate) {
            return false;
        }
        return true
    }

    const renderValidateString = () => {
        if (name.length <= 0) {
            setNameValidate("Tên người dùng không được để trống")
        } else {
            setNameValidate(null)
        }

        if (username.length <= 0) {
            setUserNameValidate("Tên tài khoản không được để trống")
        } else {
            setUserNameValidate(null)
        }

        if (password.length < 8) {
            setPasswordValidate("Mật khẩu không được ít hơn 8 kí tự")
        } else if (rePassword.trim() != password.trim()) {

            setPasswordValidate("Mat khau ko trung")
        } else {
            setPasswordValidate(null)
        }

        if (rePassword.length < 8) {
            setRePasswordValidate("Mật khẩu không được ít hơn 8 kí tự")
        } else if (rePassword.trim() != password.trim()) {
            setRePasswordValidate("Mật khẩu không trùng nhau")

        } else {
            setRePasswordValidate(null)
        }

        if (phone.length <= 0) {
            setPhoneValidate("Sđt không được để trống")
        } else {
            setPhoneValidate(null)
        }

        if (email.length <= 0) {
            setEmailValidate("Email không được để trống")
        } else {
            setEmailValidate(null)
        }
    }

    const onChangeName = (e) => {
        setName(e.target.value)
    }

    const onChangeUserName = (e) => {
        setUsername(e.target.value)
    }

    const onChangePassword = (e) => {
        setPassword(e.target.value)
    }

    const onChangeRePassword = (e) => {
        setRePassword(e.target.value)
    }

    const onChangePhoneNumber = (e) => {
        setPhone(e.target.value)
    }

    const onChangeEmailValidate = (e) => {
        setEmail(e.target.value)
    }

    const handleClickOpen = () => {
        handleClick();
    };
    const handleClose = () => {
        setOpen(false);
    };
    // phần này call API
    //const api = 'https://be-wegown.bongngotv.vip';

    const handleClick = async () => {
        renderValidateString()
        if (validate()) {
            try {
                const response = await register({
                    name,
                    username,
                    password,
                    phone,
                    email,
                    roleId: '64c784073e3d177ea3fe6317',
                })
                console.log("response", response);
                setLoading(false)
                setOpen(true);
            } catch (error) {
                console.log(error);
            }
        }
    };

    return (
        <div className='component'>
            <div className='title'>
                <Link className='logout' href="/login" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Đăng ký tài khoản</div>
            </div>
            <form action="register" className='register'>
                <TextField className='custom_input' id="standard-basic" variant="standard" label="Họ và tên" onChange={onChangeName} error={nameValidate} />
                {nameValidate && (<div className="text-red-700">{nameValidate}</div>)}
                <TextField className='custom_input' id="standard-basic" variant="standard" label="Tên đăng nhập" onChange={onChangeUserName} error={userNameValidate} />
                {userNameValidate && (<div className="text-red-700">{userNameValidate}</div>)}
                <FormControl className='custom_input' variant="standard">
                    <InputLabel htmlFor="standard-adornment-password">Mật khẩu</InputLabel>
                    <Input
                        onChange={onChangePassword}
                        error={passwordValidate}
                        id="standard-adornment-password"
                        type={showPassword ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                    {passwordValidate && (<div className="text-red-700">{passwordValidate}</div>)}
                </FormControl>
                <FormControl className='custom_input' variant="standard">
                    <InputLabel htmlFor="standard-adornment-password">Nhập lại mật khẩu</InputLabel>
                    <Input
                        onChange={onChangeRePassword}
                        error={rePasswordValidate}
                        id="standard-adornment-password"
                        type={showPassword ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <TextField
                    error={phoneValidate}
                    onChange={onChangePhoneNumber}
                    className='custom_input'
                    id="standard-number"
                    label="Số điện thoại"
                    type="number"
                    variant="standard"
                />
                <TextField onChange={onChangeEmailValidate} error={emailValidate} className='custom_input' id="standard-basic" variant="standard" label="Email" />
                <div className='btn_action'>
                    <Button className='custom-button' block onClick={handleClickOpen}>Tạo tài khoản</Button>
                    <BootstrapDialog
                        onClose={handleClose}
                        aria-labelledby="customized-dialog-title"
                        open={open}
                    >
                        <DialogTitle className='popup_success' id="customized-dialog-title">
                            Thành công
                        </DialogTitle>
                        <IconButton
                            aria-label="close"
                            onClick={handleClose}
                            sx={{
                                position: 'absolute',
                                right: 8,
                                top: 8,
                                color: (theme) => theme.palette.grey[500],
                            }}
                        >
                            <CloseIcon />
                        </IconButton>
                        <DialogContent dividers>
                            <Typography gutterBottom>
                                <p className='txt'>Chào mừng bạn đến với TeenUp!</p>
                                <p className='txt'>Đăng nhập và bắt đầu đường đua ngay thôi</p>
                            </Typography>
                            <Image
                                className='img'
                                src="/congrulation.png"
                                width={70}
                                height={70}
                                alt="teenUp"
                            />
                        </DialogContent>
                        <div className='btn'>
                            <Button className='btn_login' autoFocus onClick={handleClose}>
                                <Link className='login' href="/login" underline="always">Đăng nhập ngay</Link>
                            </Button>
                        </div>
                    </BootstrapDialog>
                </div>
            </form>
        </div>
    )
}

export default Page