'use client'
import React, { use, useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import { getUser } from '@/networks/role.service'
import userService from '../../networks/use.service'
import { useSearchParams } from 'next/navigation'
import ProgramService from "../../networks/program.service"

import "./style.css"
import { HOST } from '@/networks/constants';
import HistoryService from '../../networks/history.service';
import { useDispatch, useSelector } from 'react-redux';
import HistoryTestService from "@/networks/history-test.service"
import toast from 'react-hot-toast';
import { Modal } from 'antd';
import { useRouter } from 'next/navigation';

/**
*Trạng thái của chuo7ng trình với người dùng
* 0 : NOT_REG_HAS_RIGHT
* 1: NOT_REG_NO_RIGHT
* 2: HAS_REG_NO_RIGHT
* 3: HAS_REG_HAS_RIGHT 
 **/

export default function Page() {
  var dispatch = useDispatch()
  const router = useRouter()

  var userRedux = useSelector((state) => state.user)
  console.log('user redux', userRedux)
  var roles = userRedux.roleId
  console.log('user roleredux', roles)
  const freeProgramId = roles?.freeProgramIds
  const paidProgramIds = roles?.freeProgramId


  const [userInfo, setUserInfo] = useState();
  const [program, setProgram] = useState();
  const [history, setHistory] = useState();
  const [lessons, setLessons] = useState([]);
  const [modalVisible, setModalVisible] = useState(false)

  const searchParams = useSearchParams()
  const id = searchParams.get('id')
  var isFreeProgram = freeProgramId && freeProgramId?.includes(id)
  var isPaidProgram = freeProgramId && paidProgramIds?.includes(id)
  useEffect(() => {
    getUserInfo()

  }, [])

  const getUserInfo = async () => {
    const token = localStorage.getItem("token")
    const userInfoData = await getUser(token)
    console.log('users', userInfoData);

    try {
      if (id) {
        console.log("start get info user")


        var item = await getProgram(token);
        if (item.data?.data?.statusProgramWithUser === 1) {
          setModalVisible(true)

        }
        let listLessonFromProgarm = item.data?.data?.lessonIds ?? [];
        console.log('listLessonFromProgarm ==>', listLessonFromProgarm)
        var history = await getHistory(token, userInfoData.data.data._id)
        if (history) {
        }
        // const dataResHistoryTest = history.data?.data.reverse();
        var historyTest = await getHistoryTest(token, history.data.data[0]?._id)
        const dataResHistoryTest = historyTest.data?.data.reverse();
        console.log('dataResHistoryTest ==>', dataResHistoryTest)
        if (dataResHistoryTest && dataResHistoryTest.length > 0) {
          dataResHistoryTest.forEach(element => {
            listLessonFromProgarm = listLessonFromProgarm.map((item) => {
              if (item._id == element.lessonId?._id) {
                console.log("map lesson to history ", `${element.score} điểm`)
                return {
                  ...item,
                  result: {
                    msg: `${element.score} điểm`
                  }
                }
              } else {
                return item
              }
            })
          })
        }
        setLessons(listLessonFromProgarm)
        setHistory(history?.data?.data)
        console.log('history', history);
        console.log('program', item);
        setProgram(item)
        return
      } else {


        setUserInfo(userInfo)
        return userInfo
      }
    } catch (error) {
      console.error("Không thể lấy thông tin", error)
      return null
    }
  }

  const getProgram = async (token) => {
    return await ProgramService.getDetail(id, token)
  }
  const getHistory = async (token, userId) => {

    console.log("userinfo >>>", userInfo);
    return await HistoryService.getList({
      "userId": userId,
      "programId": id
    }, token)
  }

  const getHistoryTest = async (token, historyId) => {
    return await HistoryTestService.getList({
      historyId: historyId,
      sortBy: "updatedAt",
      sortOrder: "asc",
    }, token)
  }

  const registProgram = async () => {
    try {
      var token = localStorage.getItem('token')
      var res = await HistoryService.create({
        "userId": userRedux._id,
        "programId": program.data?.data?._id,
        "status": 0
      }, token)
      const dataRes = res.data
      console.log("dataRes", dataRes)
      if (dataRes) {
        if (program.data?.data?.statusProgramWithUser == 1) {
          setModalVisible(true)
        }
      } else {
        toast.error(res.message)
      }
      await getProgram()
    } catch {
      toast.error(res.message)
    }

  }

  return (
    <div className='container mx-auto'>
      <div className='title'>
        <Link className='logout' href="/home" underline="always">
          <ArrowBackIosIcon />
        </Link>
        <div className='txt_head'>Chi tiết chương trình</div>
      </div>

      <div >
        <div className='content' >{program?.data?.data?.description}</div>
        <div className='wrap'>
          <div className='title-1'>Chờ chút nhé... Thông tin đăng ký tham gia chương trình này của bạn đang trong quá trình xử lý.</div>
          <div className='title-2'> Trường hợp muốn bắt đầu ngay, vui lòng nhắn tin với chuyên gia tại <Link href="/chat-web">đây</Link></div>
        </div>
        <div>
          <div className='content-txt'>Chương trình học gồm các nhóm kỹ năng cơ bản giúp người học có đủ khả năng xử lý tình huống ...</div>
          <img className='img' src={`${HOST}/${program?.data?.data?.photo}`} />
          <div className='task'>{program?.data?.data?.lessonIds?.length} bài học</div>
        </div>
        {lessons?.map((x, key) => {
          var his = history[0];
          return (
            <div className="list" key={key}>
              <div className="about">
                <button className="about-item"
                  onClick={() => {
                    if (program?.data?.data.statusProgramWithUser == 3) {
                      router.push(`/lesson-detail?id=${x._id}&historyId=${his?._id ?? ''}`)
                    }
                  }}
                >
                  <img
                    className='img_list'
                    src={`${HOST}/${x?.photo}`}
                    width={50}
                    height={50}
                    alt="teenUp" />
                  <div className="gr-line-item">
                    <div className="line-title-app">{x.title}</div>
                    <div className="task-description-app">{x.description}</div>
                  </div>
                </button>
              </div>
            </div>
          );
        })}
      </div>
      {
        !userInfo && program?.data?.data.statusProgramWithUser == 0 ? (
          <button onClick={() => registProgram()} className='btn fixed bottom-4'>{
            "Tham gia ngay miễn phí"
          }</button>

        ) : null
      }

      {
        !userInfo && program?.data?.data.statusProgramWithUser == 1 ? (
          <button onClick={() => registProgram()} className='btn fixed bottom-4'>{
            "Đăng ký ngay"
          }</button>

        ) : null
      }

      <Modal
        open={modalVisible}
        title="Đăng ký thành công"
        footer={
          <div className='w-full flex justify-center'>
            <button onClick={() => setModalVisible(false)} className='bg-[#F39C12] text-white p-2 rounded-xl'>
              Xem nội dung khác
            </button>
            <div className='w-5' />
            <button onClick={() => setModalVisible(false)} className=''>
              Nhắn tin
            </button>
          </div>
        }
      >
        <p>
          Thông tin đăng ký của bạn đã được gửi đến các chuyên gia và đang được xử lý rồi.\n\nTrong thời gian chờ xác nhận, bạn xem thử các nội dung khác nha. Trường hợp muốn tham gia chương trình này ngay lập tức, hãy nhắn tin trực tiếp với chuyên gia để được hỗ trợ nhé
        </p>
      </Modal>
    </div >

  )
}
