'use client'

import React, { useEffect, useState } from 'react'
import { Avatar, Stack, Link } from '@mui/material';
import NotificationsIcon from '@mui/icons-material/Notifications';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import StarIcon from '@mui/icons-material/Star';
import Image from 'next/image'
import Slider from "react-slick";
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import { getUser } from '@/networks/role.service'
import roleService from '../../networks/role.service'
import HistoryService from '../../networks/history.service'
import Protected from '../providers/protected';
import { HOST } from '@/networks/constants';
import ScoreService from '../../networks/score.service';
import ProgramService from "../../networks/program.service";
import { useDispatch, useSelector } from 'react-redux';
import { setAuthUserInfo } from '@/redux/auth/authSlice';
// import Footer from '../footer/footer'

// import "./style.css" //không sử dụng css cho riêng trang nữa mà ăn theo css của masterpage thôi
// import axios from 'axios';

const Responsive = ({ listMyPrograms }) => {
  var slickSettings = {
    arrows: false,
    infinite: false,
    speed: 300,
    slidesToShow: 8,
    slidesToScroll: 8,
    initialSlide: 8,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 820,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
          // initialSlide: 3
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
          // initialSlide: 2
        }
      }
    ]
  };

  return (
    <>
      <link
        rel="stylesheet"
        type="text/css"
        charSet="UTF-8"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
      />

      <div className="slider pb-8">
        <Slider {...slickSettings}>
          {listMyPrograms?.map((x, key) => {
            // console.log(x)
            return (
              <a key={key} className="slider-item" href={`/program-detail?id=${x._id}`}>
                <div className='h-[120px] overflow-hidden'>
                  <img
                    className='img-list'
                    src={`${HOST}/${x?.photo}`}
                    alt="teenUp" />
                </div>
                <div className="gr-task">
                  <div className='text-over'>{x.title}</div>
                  <div className="task-all">
                    <div className="task">{x.lessonIds.length} bài học</div>
                    {x.processing ? <div className="percent">{x.processing}%</div> : null}
                  </div>
                </div>
              </a>
            );
          })}
        </Slider>
      </div>
    </>
  )
}


function Page(string) {



  ///sẽ sử dụng redux để load userInfo từ header chứ ko gọi trực tiếp tại đây
  const [userInfo, setUserInfo] = useState()
  const [score, setScore] = useState(0)
  const [level, setLevel] = useState("")
  const [listTrendingPrograms, setListTrendingPrograms] = useState()
  const [listMyPrograms, setListMyPrograms] = useState()

  useEffect(() => {
    getUserInfo()
  }, [])

  useEffect(() => {
    getMyProgram()
    getScore()
  }, [userInfo])

  const getUserInfo = async () => {
    const token = localStorage.getItem("token")
    const user = await getUser(token)
    // console.log(user);
    if (user) {
      setUserInfo(user?.data?.data)

      // console.log("user", user);
    }
  }

  const getScore = async () => {
    if (!userInfo) return
    try {
      const accessToken = localStorage.getItem('token')
      const res = await ScoreService.getDetailByUserId(userInfo._id, accessToken)
      const data = res.data
      // console.log("data getScore ---> ", data)
      if (data) {
        setScore(data.data.score)
        setLevel(data.data.levelId.title)
        // setLevelIcon(data.levelId.photo)
      } else {
        console.log("data getScore ---> ", data)

      }

    } catch (error) {
      console.error("error", error)
    }
  }

  const getMyProgram = async () => {
    try {
      //program by role ==>
      const accessToken = localStorage.getItem('token')
      const res = await roleService.getRoleForUser(userInfo?.roleId?._id, accessToken)
      const data = res.data
      // console.log("program by role ==>", data)

      //program by user register and assigned ==>
      const resProgramRegist = await HistoryService.getList({ userId: userInfo?._id }, accessToken)
      // console.log("resProgramRegist", resProgramRegist);
      const dataProgramRegist = resProgramRegist.data.data.map((item) => {
        return {
          ...item.programId,
          processing: item.processing
        }
      })
      // console.log("dataProgramRegist ==>", dataProgramRegist)
      const result = dataProgramRegist
      data.data.freeProgramIds.forEach(element => {
        const exist = result.find((item) => item._id == element._id)
        if (!exist) {
          result.push(element)
        }
      })
      data.data.paidProgramIds.forEach(element => {
        const exist = result.find((item) => item._id == element._id)
        if (!exist) {
          result.push(element)
        }
      })

      setListMyPrograms(result.sort((item1, item2) => {
        return item1.processing - item2.processing
      }))
      getProgram(result)
    } catch (error) {
      console.error("error getMyProgram", error)
    }
  }

  const getProgram = async (notInList) => {
    try {
      const accessToken = localStorage.getItem('token')
      // console.log(accessToken);
      if (accessToken) {
        const res = await ProgramService.getList(accessToken)
        const data = res.data
        // console.log("data", data);
        const list = []
        data.data.forEach(element => {
          const exist = notInList.find((item) => item._id == element._id)
          if (!exist) {
            list.push(element)
          }
        })
        setListTrendingPrograms(list)
      }

    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Protected>
      <div className='component'>

        <div className="container mx-auto home-board-section">
          {/* <div className="header">
                        <div className="gr">
                            {userInfo && (<img
                                className='avt'
                                src={`${HOST}/${userInfo?.photo}`}
                                alt="#"
                                width={40}
                                height={40}
                            />)}
                            <div className="intro">Xin chào {userInfo?.name}</div>
                        </div>
                        <Link className='logout' href="/notification" underline="always">
                            <NotificationsIcon className="notify" />
                        </Link>
                    </div> */}
          <div className="menu">
            <div className="group">
              <div className="group_item">
                <StarIcon className="icon" />
                <div className="level">{level}</div>
              </div>
              <div className="point">{score} điểm</div>
            </div>
            <div className="nav">
              <a className='nav-item' href="/timeline" underline="always">
                <Image
                  className='img'
                  src="/lotrinh.png"
                  width={40}
                  height={40}
                  alt="teenUp"
                />
                Lộ trình
              </a>
              <a className='nav-item' href="/ratings" underline="always">
                <Image
                  className='img'
                  src="/xephang.png"
                  width={40}
                  height={40}
                  alt="teenUp"
                />
                Xếp hạng
              </a>
              <a className='nav-item' href="/story" underline="always">
                <Image
                  className='img'
                  src="/nhatky.png"
                  width={40}
                  height={40}
                  alt="teenUp"
                />
                Nhật ký
              </a>
            </div>
          </div>
        </div>
        <div className="detail-item">
          <div className="home-section">
            <div className="programme">Chương trình của tôi</div>
            <a href='/my-program' className="view">Xem toàn bộ</a>
          </div>
          <Responsive listMyPrograms={listMyPrograms} />
          <div className="detail2">
            <div className="programme">Chương trình phổ biển</div>
            <a href='/my-program-list' className="view">Xem toàn bộ</a>
          </div>
          {listTrendingPrograms?.map((x, key) => (
            <div className="list" key={key}>
              <a className="about" href="/program-detail">
                <div className="about-item">
                  <Image
                    className='img_list'
                    src={`${HOST}/${x?.photo}`}
                    width={50}
                    height={50}
                    alt="teenUp"
                  />
                  <div className="line-item">
                    <div className="line-1">{x.title}</div>
                    <div className="task-ellip">{x.description}</div>
                    <div className="task">{x.lessonIds.length} bài học</div>
                  </div>
                </div>
                <ArrowCircleRightIcon className="arrow" />
              </a>
            </div>
          ))}
        </div>

      </div>
      <div className='h-[70px] bg:-white w-full' />
      {/* <Footer /> */}
    </Protected>
  )
}

export default Page
