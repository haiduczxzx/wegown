'use client'
import React, { useEffect, useState } from 'react'
import { useSearchParams } from "next/navigation"
import axios from 'axios';
import { HOST } from '@/networks/constants';

export default function Page() {
    const [lesson, setLesson] = useState()
    const searchParams = useSearchParams()
    const id = searchParams.get('id')
    const getLesson = async (accessToken) => {
        return await axios.get(`${HOST}/lesson/${id}`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
    }
    useEffect(() => {
        var token = localStorage.getItem("token")
        getLesson(token).then((v) => {
            console.log('lesson', v);
            setLesson(v?.data?.data)
        })



    }, [])
    return (
        <div className='component-perform'>
            <iframe className='we' src={lesson?.exerciseIds[0]?.linkQuiz} >Thực hiện thử thách</iframe>
        </div>
    )
}