'use client'
import React, { useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useRouter } from 'next/navigation';
import SearchIcon from '@mui/icons-material/Search';
import {
    Link,
    FormControl,
    InputAdornment,
    TextField,
    createStyles,
    makeStyles
} from '@mui/material';
import { BottomSheet } from 'react-spring-bottom-sheet'
import 'react-spring-bottom-sheet/dist/style.css'
import { getUser } from '@/networks/role.service'
import { DatePicker } from 'antd';
import HistoryTestService from '../../networks/history-test.service';
import moment from 'moment'
const { RangePicker } = DatePicker;

export default function Page() {
    const router = useRouter();
    const [showClearIcon, setShowClearIcon] = useState("none");
    const [open, setOpen] = useState(false)
    const [userInfo, setUserInfo] = useState()
    const [listData, setListData] = useState()
    const [params, setParams] = useState({
        page: 1,
        pageSize: 100,
        isDelete: false,
        status: "",
        sortBy: "updatedAt",
        sortOrder: "desc",
        keyword: "",
        exerciseId: "",
        hasMakeScoreDone: "",
        userId: "",
        endDate: "",
        startDate: "",
    })

    // useEffect(() => {
    //     getUserInfo()
    // }, [])

    useEffect(() => {
        getPrograms()
    }, [])
    useEffect(() => {
    }, [listData])

    const handleClick = () => {
        router.push('/manage')
    }

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)

        }
    }

    const getPrograms = async () => {
        try {
            const token = localStorage.getItem("token")
            await getUserInfo()
            // console.log("user", userInfo);
            // console.log("params", params)
            //if (!userInfo) return
            const res = await HistoryTestService.getList(params, token)
            const data = res.data
            // console.log("data", data);
            setListData(data?.data.filter((item) => item.listQuestionIds && item.listQuestionIds.length > 0))
        } catch (error) {
            console.log("Error getListProgram", error)
        }

    }

    return (
        <div className='component-point-management'>
            <div className='gr-list-user'>
                <ArrowBackIosIcon onClick={handleClick} />
                <div className='title-list-user'>Quản lý điểm</div>
            </div>
            <div>
                <div className="search-manage-item">
                    <TextField
                        placeholder="Tìm kiếm nội dung"
                        className="search-point"
                        size="small"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    style={{ display: showClearIcon }}
                                ></InputAdornment>
                            )
                        }}
                    />
                    <button onClick={() => setOpen(true)}><img src="/filter.png" alt="" /></button>
                    <BottomSheet open={open} onBlur={() => setOpen(false)}>
                        <button><RangePicker /></button>
                        <div>Trạng thái</div>
                        <div>Thử thách</div>
                        <div>Người thực hiện</div>
                        <button>Tìm Kiếm</button>
                    </BottomSheet>
                </div>
            </div>
            <div className="gr-management">
                <button className="btn-management">Tất cả</button>
                <button className="btn-management">Chưa chấm</button>
                <button className="btn-management">Đã chấm</button>
            </div>
            {
                listData?.map((x, key) => {
                    var date = new Date(x?.createdAt);
                    var dateString = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
                    return (
                        <div className='name-management' key={key}>
                            <div className='gr-name-management'>
                                <div className='text-management'>{x?.exerciseId?.title}</div>
                                <div className='label-management'>Người dùng: {x?.userId?.name}</div>
                                <div className='label-management'>Thời gian nộp: {dateString}</div>
                                <div className='status'>TN: {x?.score} - TL: {x.hasMakeScoreDone == true ? x.scoreTL : 'Chưa chấm'}</div>
                            </div>
                            <Link href="/exercise-name">
                                <img src="/right.png" alt="" />
                            </Link>
                        </div>
                    );
                })
            }
        </div>
    )
}
