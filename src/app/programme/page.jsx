'use client'
import "./style.css"
import React from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import Image from 'next/image'

function Page() {
    return (
        <div className="component">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Chương trình của A</div>
            </div>
            <div className="total">
                <div className="group">
                    <div className="item">
                        <div className="num">20</div>
                        <div className="text">Tổng số</div>
                    </div>
                    <div className="item">
                        <div className="num">20</div>
                        <div className="text">Đang học</div>
                    </div>
                    <div className="item">
                        <div className="num">20</div>
                        <div className="text">Chưa học</div>
                    </div>
                    <div className="item">
                        <div className="num">20</div>
                        <div className="text">Hoàn thành</div>
                    </div>
                </div>
            </div>
            <div className="list">
                <div className="about">
                    <div className="about-item">
                        <Image
                            className='img_list'
                            src="/slider.png"
                            width={50}
                            height={50}
                            alt="teenUp"
                        />
                        <div className="line-item">
                            <div className="line-1">Kỹ năng số 1</div>
                            <div className="task">[Mô tả ngắn gọn chương trình]</div>
                            <div className="task">15 bài học</div>
                        </div>
                    </div>
                    <div className="loading">
                        <div className="persent">50%</div>
                        <Image
                            className='img'
                            src="/loading.png"
                            width={30}
                            height={30}
                            alt="teenUp"
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Page