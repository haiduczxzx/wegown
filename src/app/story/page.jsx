'use client'
import React, { useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import Image from 'next/image';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
// import { Button } from 'antd';
import Button from '@mui/material/Button';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineOppositeContent, {
    timelineOppositeContentClasses,
} from '@mui/lab/TimelineOppositeContent';
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { DatePicker, Space } from "antd";
import { getUser } from '@/networks/role.service'
import LevelService from '../../networks/level.service';
import ScoreService from '../../networks/score.service';
import HistoryService from '../../networks/history.service';
import HistoryTestService from '../../networks/history-test.service';
import moment from 'moment'
import "./style.css"

dayjs.extend(customParseFormat);
const { RangePicker } = DatePicker;
const dateFormat = "YYYY/MM/DD";
function Page() {
    // const [hiden, setHiden] = useState(false)
    const [disableButton, setDisableButton] = React.useState(false)
    const [idNotDisable, setIdNotDisable] = React.useState(null)
    const [showDatePicker, setShowDatePicker] = React.useState(false);
    const [userInfo, setUserInfo] = useState()
    const [level, setLevel] = useState()
    const [score, setScore] = useState()
    const [position, setPosition] = useState(0)
    const [name, setName] = useState("")
    const [listData, setListData] = useState()
    const [numberOfFinishedProgram, setNumberOfFinishedProgram] = useState()
    const [numberOfProgressProgram, setNumberOfProgressProgram] = useState()
    const [numberOfFinishLesson, setNumberOfFinishLesson] = useState()
    const [params, setParams] = useState({
        page: 1,
        pageSize: 10000,
        isDelete: false,
        sortBy: "updatedAt",
        sortOrder: "desc",
        endDate: moment().set("date", 30),
        startDate: moment().set("date", 1),
        userId: null,
    })
    //   
    const [startDate, setStartDate] = useState()
    const [endDate, setEndDate] = useState()
    const [type, setType] = useState(-1)
    const [openDatePicker, setOpenDatePicker] = useState(false)
    const [displayedDate, setDisplayedDate] = useState(moment())
    const setDates = (dates) => {
        console.log("dates", dates)
        if (dates.startDate)
            setStartDate(dates.startDate)
        if (dates.endDate) {
            setEndDate(dates.endDate)
            setTimeout(() => {
                setOpenDatePicker(false)
                setParams({
                    ...params,
                    startDate: startDate,
                    endDate: dates.endDate
                })
            }, 300)
        }
        if (dates.displayedDate != undefined) {
            setDisplayedDate(dates.displayedDate)
        }
    }

    useEffect(() => {
        getUserInfo().then(async (v) => {
            await getDefaultLevel()
            // getListScore()
        })
    }, [])

    useEffect(() => {
        getListScore()
    }, [level])

    useEffect(() => {
        if (!userInfo) return
        setParams({
            ...params,
            userId: userInfo._id
        })
        getScore()
        getListHistory()
        getListHistoryTestForHeader()
    }, [userInfo])


    useEffect(() => {
        getListHistoryTest()
    }, [params])
    // useEffect(() => {
    //     getListScore()
    // }, [level])

    const handleClick = (event) => {
        // setHiden(true)
        const id = event.target.id;
        setDisableButton(!disableButton)
        setIdNotDisable(id)
        // console.log("press all")
        setType(-1)
        const start = moment().set("date", 1).toDate()
        const end = moment().set("date", 30).toDate()
        setStartDate(start)
        setEndDate(end)
        setParams({
            ...params,
            startDate: start,
            endDate: end
        })
    }

    const handClickMonth = () => {
        // console.log("press Cơ bản")
        const thisMonth = moment().get("month")
        let lastMonth = thisMonth - 1
        if (lastMonth <= 0) lastMonth = 1
        const start = moment().set("date", 1).set("month", lastMonth)
        const end = moment().set("date", 30).set("month", lastMonth)
        setStartDate(start)
        setEndDate(end)
        setType(0)
        setParams({
            ...params,
            startDate: start,
            endDate: end
        })
    }

    const handClickCustomer = () => {
        setType(1)
        setOpenDatePicker(true)
    }

    const handleButtonClick = () => {
        // console.log("press hight")
        setShowDatePicker(true);
        // setOpenDatePicker(false)
        // setDates()
    };

    const getScore = async () => {
        if (!userInfo) return
        try {
            const accessToken = localStorage.getItem('token')
            const res = await ScoreService.getDetailByUserId(userInfo._id, accessToken)
            const data = res.data
            // console.log("data getScore ---> ", data)
            if (data) {
                setScore(data.data.score)
                setLevel(data.data.levelId)
            } else {
                // console.log("data getScore ---> ", data)
                await getDefaultLevel()
            }
        } catch (error) {
            console.error("error", error)
        }
    }

    // const getLevel = async () => {
    //     try {
    //         const accessToken = localStorage.getItem('token')
    //         const res = await LevelService.getList({ pageSize: 10000 }, accessToken)
    //         const data = res.data
    //         // console.log(data);
    //         if (data) {
    //             // console.log("level?._id", level)
    //             setListLevel(data.data.map((item) => {
    //                 return {
    //                     ...item,
    //                     active: item._id == level?._id
    //                 }
    //             }).reverse())
    //         }
    //     } catch (error) {
    //         console.error('getRank', error)
    //     }
    // }

    const getListHistory = async () => {
        try {

            var token = localStorage.getItem('token')
            // console.log({ token });
            const res = await HistoryService.getList({
                userId: userInfo?._id,
            }, token)
            const list = res.data
            // console.log("list", list);
            const listFinish = list.data.filter((item) => (item.processing && item.processing == 100))
            const listOnProgress = list.data.filter((item) => (item.processing && item.processing != 100 && item.processing != 0))
            setNumberOfFinishedProgram(listFinish?.length)
            setNumberOfProgressProgram(listOnProgress?.length)
        } catch (error) {
            console.error('getListHistory', error)
        }
    }

    const getListHistoryTestForHeader = async () => {
        try {
            var token = localStorage.getItem('token')
            const res = await HistoryTestService.getList({
                page: 1,
                pageSize: 10000,
                isDelete: false,
                sortBy: "updatedAt",
                sortOrder: "desc",
                userId: userInfo?._id,
            }, token)
            const list = res.data
            // console.log("list", list);
            setNumberOfFinishLesson(list.data?.length)
        } catch (error) {
            console.error('getListHistoryTestForHeader', error)
        }
    }

    const getDefaultLevel = async () => {
        try {
            const accessToken = localStorage.getItem('token')
            const res = await LevelService.getList({ pageSize: 10000 }, accessToken)
            const data = res.data
            // console.log('data', data);
            if (data) {
                const firstLevel = data.data.find((item) => item.minScore == 0)
                // setLevel(firstLevel)
                // console.log('firstLevel', firstLevel);
                if (firstLevel) {
                    setName(firstLevel?.title)
                }
            }
        } catch (error) {
            console.error('get rank', error)
        }
    }

    const getListScore = async () => {
        try {
            const accessToken = localStorage.getItem('token')
            const res = await ScoreService.getList({ pageSize: 10000 }, accessToken)
            const data = res.data
            // console.log("res.data", data);
            if (data) {
                data.data.forEach((item, index) => {
                    if (item.userId._id == userInfo?._id) {
                        setPosition(index + 1)
                    }
                })
            }
        } catch (error) {
            console.error('get score', error)
        }
    }

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    const getListHistoryTest = async () => {
        try {
            const token = localStorage.getItem("token")
            // if (!params.userId) return
            const res = await HistoryTestService.getLog({
                ...params
            }, token)
            // console.log("params", params);
            // console.log(res);
            const list = res.data
            // console.log("list", list);
            setListData(list)
        } catch (error) {
            console.error('getListHistory', error)
        }
    }

    const LogType = {
        JoinProgram: 'JoinProgram',
        FinishLessonWithScore: 'FinishLessonWithScore',
        FinishLessonWithoutScore: 'FinishLessonWithoutScore',
        UpdateScoreTL: 'UpdateScoreTL',
        FinishProgram: 'FinishProgram',
        Default: 'Default',
    }
    const getContent = (item) => {
        switch (item?.type) {
            case LogType.JoinProgram:
                return `Tham gia chương trình ${item?.program?.title}`
            case LogType.FinishLessonWithScore:
                return `Hoàn thành bài học ${item.lesson?.title} và nhận ${item?.score} điểm`
            case LogType.FinishLessonWithoutScore:
                return `Hoàn thành bài học ${item.lesson?.title}`
            case LogType.UpdateScoreTL:
                return `Nhận ${item?.score} điểm từ bài tập tự luận tại bài ${item?.lesson?.title ? item?.lesson?.title : ""}`
            case LogType.FinishProgram:
                return `Hoàn thành chương trình ${item?.program?.title}`
            default:
                return ''
        }
    }

    return (
        <div className="component-story">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Nhật ký học tập</div>
            </div>
            <div className="info">
                <Image
                    className='avt'
                    src="/star.png"
                    width={50}
                    height={50}
                    alt="teenUp"
                />
                <div className="name">{userInfo?.name}</div>
                <div className="gr-open">
                    <div className="total">Điểm: {score}</div>
                    <div className="total">Xếp hạng: {position + 1}</div>
                    <div className="total-end">Cấp: {name}</div>
                </div>
            </div>
            <div className="process">
                <div className="open-popup">
                    <div className="label">Tiến trình học tập</div>
                    <Link className='logout-popup' href="/my-program" underline="always">
                        <ChevronRightIcon />
                    </Link>
                </div>
                <div className="task">
                    <div className="txt">Bài học đã hoàn thành</div>
                    <div className="num">{numberOfFinishLesson}</div>
                </div>
                <div className="task">
                    <div className="txt">Chương trình đã hoàn thành</div>
                    <div className="num">{numberOfFinishedProgram}</div>
                </div>
                <div className="task">
                    <div className="txt">Chương trình đang học</div>
                    <div className="num">{numberOfProgressProgram}</div>
                </div>
                <div className="completion">
                    <Image
                        className='avt-story'
                        src="/vector.png"
                        width={16}
                        height={16}
                        alt="teenUp"
                    />
                    <div className="content">Hoàn thành thêm 3 chương trình nữa, bạn sẽ lọt top 10% người học</div>
                </div>
            </div>
            <div className="story">
                <div className="label-str">Lịch sử học tập</div>
                <div className="btn-item-app">
                    <Button
                        id='1'
                        className="btn-story-app"
                        variant="contained"
                        onClick={handleClick}
                    >Tháng này</Button>
                    <Button
                        id='2'
                        className="btn-story-app"
                        variant="contained"
                        onClick={handClickMonth}>Tháng trước</Button>
                    <div
                        id='3'
                        onClick={handClickCustomer}
                        disabled={disableButton && idNotDisable != '3'}>
                        <Button className="btn-story-app" variant="contained" onClick={handleButtonClick}>Tùy chỉnh</Button>
                        {
                            showDatePicker && <Space>
                                <RangePicker
                                    endDate={endDate}
                                    displayedDate={displayedDate}
                                    open={openDatePicker}
                                    onChange={setDates}
                                    className="calender"
                                    format={dateFormat}
                                    picker="date" />
                            </Space>
                        }
                    </div>
                </div>
                {listData?.data?.map((x, key) => (
                    <div className="timeline" key={key}>
                        <Timeline position="right"
                            sx={{
                                [`& .${timelineOppositeContentClasses.root}`]: {
                                    flex: 0.3,
                                },
                            }}
                        >
                            <TimelineItem>
                                <TimelineOppositeContent color="text.secondary">
                                    <div className="gr">
                                        <div className="level-story">{moment(x.updatedAt).format('hh:mm:ss')}{"\n"}{moment(x.updatedAt).format('DD/MM/YYYY')}</div>
                                        {/* <div className="level">10/10/2022</div> */}
                                    </div>
                                </TimelineOppositeContent>
                                <TimelineSeparator>
                                    <TimelineConnector />
                                    <TimelineDot />
                                    <TimelineConnector />
                                </TimelineSeparator>
                                <TimelineContent>
                                    <div className="point-story">{getContent(x)}</div>
                                </TimelineContent>
                            </TimelineItem>
                        </Timeline>
                    </div>
                ))}
            </div>
        </div>
    )
}
export default Page