'use client'
import "./style.css"
import React from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import Image from 'next/image'

function Page() {
    return (
        <div className="component">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Chi tiết chương trình</div>
            </div>
            <div className="detail">
                <div className="content">Kỹ năng số 1</div>
                <div className="txt">Chương trình học gồm các nhóm kỹ năng cơ bản giúp người học có đủ khả năng xử lý tình huống ...</div>
                <Image
                    className='img'
                    src="/detail.png"
                    width={500}
                    height={50}
                    alt="teenUp"
                />
                <div className="group">
                    <div className="exercise">15 bài học</div>
                    <div className="done">Đã hoàn thành: 3/15</div>
                </div>
                <div className="list">
                    <div className="about">
                        <div className="about-item">
                            <Image
                                className='img_list'
                                src="/slider.png"
                                width={50}
                                height={50}
                                alt="teenUp"
                            />
                            <div className="line-item">
                                <div className="line-1">Tên bài học 1</div>
                                <div className="task">[Mô tả bài học]</div>
                            </div>
                        </div>
                        <div className="loading">
                            <Image
                                className='img'
                                src="/loading.png"
                                width={20}
                                height={20}
                                alt="teenUp"
                            />
                            <div className="scores">8/10 điểm</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Page