'use client'
import React from 'react'
import {
    Link
} from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import "./style.css"

export default function Page() {
    return (
        <div className='container mx-auto'>
            <div className='title'>
                <Link className='logout' href="/information" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Thiết lập tài khoản</div>
            </div>
            <div className='detail'>
                <a href='/change-password' className='option-detail'>Thay đổi mật khẩu</a>
                <a className='option-detail'>Xóa tài khoản</a>
            </div>
        </div>
    )
}
