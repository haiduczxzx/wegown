'use client'

import React, { useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import Image from 'next/image';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import axios from 'axios';
import LevelService from '../../networks/level.service';
import ScoreService from '../../networks/score.service';
import { getUser } from '@/networks/role.service'
import { HOST } from "@/networks/constants";
import "./style.css"

function Page() {
    const [level, setLevel] = useState()
    const [userInfo, setUserInfo] = useState()
    const [listLevel, setListLevel] = useState()
    const [score, setScore] = useState()

    useEffect(() => {
        getUserInfo()
    }, [])

    useEffect(() => {
        getLevel()
    }, [level])

    useEffect(() => {
        getScore()
    }, [userInfo])

    useEffect(() => {
        console.log("listLevel", listLevel)
    }, [listLevel])

    const getScore = async () => {
        console.log({ userInfo });
        if (!userInfo) return
        try {
            const accessToken = localStorage.getItem('token')
            const res = await ScoreService.getDetailByUserId(userInfo._id, accessToken)
            // console.log("data getScore ---> ", res)

            const data = res.data
            if (data) {
                setScore(data.score)
                setLevel(data.data.levelId)
            } else {
                // console.log("data getScore ---> ", data)
            }
        } catch (error) {
            console.error("error", error)
        }
    }

    const getLevel = async () => {
        try {
            const accessToken = localStorage.getItem('token')
            const res = await LevelService.getList({ pageSize: 10000 }, accessToken)
            const data = res.data
            // console.log(data);
            if (data) {
                console.log("level?._id", level)
                setListLevel(data.data.map((item) => {
                    return {
                        ...item,
                        active: item._id == level?._id
                    }
                }).reverse())
            }
        } catch (error) {
            console.error('getRank', error)
        }
    }

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        if (user) {
            setUserInfo(user?.data?.data)
        }
    }

    return (
        <div className="component-timeline">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Lộ trình</div>
            </div>
            <div className="timeline">
                <Timeline position="right" >

                    {listLevel?.map((i, key) => (
                        <TimelineItem key={key}>
                            <TimelineOppositeContent color="text.secondary">
                                <div className="gr">
                                    <div className="level">{i.title}</div>
                                    <img
                                        className='img'
                                        src={`${HOST}/${i.photo}`}

                                        width={30}
                                        height={20}
                                        alt="teenUp"
                                    />
                                </div>
                            </TimelineOppositeContent>
                            <TimelineSeparator className="custom">
                                <TimelineDot className={i.active ? 'a' : 'b'} />
                                <TimelineConnector />
                            </TimelineSeparator>
                            <TimelineContent>
                                <div className="point">{i.minScore} điểm</div>
                            </TimelineContent>
                        </TimelineItem>
                    ))}
                </Timeline>
            </div>
        </div>
    )
}
export default Page