"use client"
import React from 'react'
import Script from 'next/script'

export default function Page() {
  return (
    <div>
      <div id="fb-root"></div>

      <div id="fb-customer-chat" class="fb-customerchat">
      </div>

      <Script id='1'>
        {`
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "2363660297251223");
        chatbox.setAttribute("attribution", "biz_inbox");
        `}
      </Script>

      <Script id='2'>
        {`
        window.fbAsyncInit = function() {
          FB.init({
            xfbml: true,
            version: 'v18.0'
          });
      };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
        `}
      </Script>
    </div>
  )
}