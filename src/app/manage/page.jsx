'use client'
import React, { useEffect, useState } from 'react'
import { getUser } from '@/networks/role.service'
import { HOST } from '@/networks/constants'
import { useRouter } from 'next/navigation';

export default function Page() {
    const router = useRouter()
    const [userInfo, setUserInfo] = useState()

    useEffect(() => {
        getUserInfo()
    }, [])

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    const handleClick = () => {
        router.push('/list-user')
    }

    const handleClickPoint = () => {
        router.push('/point-management')
    }

    return (
        <div className='container-manage'>
            <div className='manage-infor'>
                <img
                    src={`${HOST}/${userInfo?.photo}`}
                    // src='star.png'
                    alt="#"
                    width={50}
                    height={50}
                    className='avt-manage-infor' />
                <div className='name-manage-infor'>{userInfo?.name}</div>
            </div>
            <div onClick={handleClick} className='information-manage-app'>
                <div className='manage-task'>Quản lý nhật ký học tập</div>
            </div>
            <div className='information-manage'>
                <div onClick={handleClickPoint} className='manage-task'>Quản lý điểm</div>
            </div>
        </div>
    )
}
