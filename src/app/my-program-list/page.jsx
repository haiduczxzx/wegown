'use client'
import "./style.css"
import React, { useState, useEffect } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import {
    Link,
    FormControl,
    InputAdornment,
    TextField,
    createStyles,
    makeStyles
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { getUser } from '@/networks/role.service'
import roleService from '../../networks/role.service'
import HistoryService from '../../networks/history.service'
import ProgramService from "../../networks/program.service";
import { HOST } from "@/networks/constants";

export default function Page() {

    const [showClearIcon, setShowClearIcon] = useState("none");
    const [userInfo, setUserInfo] = useState();
    const [listMyPrograms, setListMyPrograms] = useState()
    const [listTrendingPrograms, setListTrendingPrograms] = useState()

    useEffect(() => {
        getUserInfo()
    }, [])

    useEffect(() => {
        getMyProgram()
    }, [userInfo])

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    const getMyProgram = async () => {
        try {
            //program by role ==>
            const accessToken = localStorage.getItem('token')
            const res = await roleService.getRoleForUser(userInfo?.roleId?._id, accessToken)
            const data = res.data

            //program by user register and assigned ==>
            const resProgramRegist = await HistoryService.getList({ userId: userInfo?._id }, accessToken)
            const dataProgramRegist = resProgramRegist.data.data.map((item) => {
                return {
                    ...item.programId,
                    processing: item.processing
                }
            })
            const result = dataProgramRegist
            data.data.freeProgramIds.forEach(element => {
                const exist = result.find((item) => item._id == element._id)
                if (!exist) {
                    result.push(element)
                }
            })
            data.data.paidProgramIds.forEach(element => {
                const exist = result.find((item) => item._id == element._id)
                if (!exist) {
                    result.push(element)
                }
            })
            setListMyPrograms(result.sort((item1, item2) => {
                return item1.processing - item2.processing
            }))
            getProgram(result)
        } catch (error) {
            console.error("error getMyProgram", error)
        }
    }

    const getProgram = async (notInList) => {
        try {
            const accessToken = localStorage.getItem('token')
            // console.log(accessToken);
            if (accessToken) {
                const res = await ProgramService.getList(accessToken)
                const data = res.data
                console.log("data", data);
                const list = []
                data.data.forEach(element => {
                    list.push(element)
                    // const exist = notInList.find((item) => false)//item._id == element._id)
                    // if (!exist) {
                    //     list.push(element)
                    // }
                })
                setListTrendingPrograms(list)
            }

        } catch (error) {
            console.error(error)
        }
    }

    return (
        <div className="container">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Chương trình</div>
            </div>
            <div className="list-item">
                <div className="search-item">
                    <TextField
                        placeholder="Tìm kiếm nội dung"
                        className="search"
                        size="small"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    style={{ display: showClearIcon }}
                                ></InputAdornment>
                            )
                        }}
                    />
                </div>
                <div className="total">
                    <button className="btn">Tất cả</button>
                    <button className="btn">Cơ bản</button>
                    <button className="btn">Nâng cao</button>
                </div>
                {listTrendingPrograms?.map((x, key) => (
                    <div className="list" key={key}>
                        <div className="about">
                            <div className="about-item">
                                <img
                                    className='img_list'
                                    src={`${HOST}/${x?.photo}`}
                                    width={50}
                                    height={50}
                                    alt="teenUp"
                                />
                                <div className="line-item">
                                    <div className="line-1">{x.title}</div>
                                    <div className="task-ellip">{x.description}</div>
                                    <div className="task">{x.lessonIds.length} bài học</div>
                                </div>
                            </div>
                            <div className="loading">
                                <img
                                    className='img-vector-app'
                                    src="/right.png"
                                    alt="teenUp"
                                />
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}
