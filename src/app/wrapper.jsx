'use client'
import Header from "@/app/header/header"
import Footer from "@/app/footer/footer"
import { usePathname, useSearchParams } from "next/navigation"
import StoreProvider from "@/redux/reduxProvider"
import AuthProvider from "./providers/AuthProvider"

const Wrapper = ({ children }) => {
  const pathname = usePathname()
  const searchParams = useSearchParams()

  return (
    <div className='wrapper relative'>
      <StoreProvider>
        <AuthProvider>
          <Header pathname={pathname} />
          {children}
          <Footer pathname={pathname} />
        </AuthProvider>
      </StoreProvider>
    </div>
  )
}

export default Wrapper
