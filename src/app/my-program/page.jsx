'use client'
import "./style.css"
import React, { useState, useEffect } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import SearchIcon from '@mui/icons-material/Search';
import {
    Link,
    FormControl,
    InputAdornment,
    TextField,
    createStyles,
    makeStyles
} from '@mui/material';
import Image from 'next/image'
import { getUser } from '@/networks/role.service'
import roleService from '../../networks/role.service'
import HistoryService from '../../networks/history.service'
import ProgramService from "../../networks/program.service";
import { HOST } from "@/networks/constants";

function Page() {
    const [showClearIcon, setShowClearIcon] = useState("none");
    const [userInfo, setUserInfo] = useState();
    const [listMyPrograms, setListMyPrograms] = useState()
    const [listTrendingPrograms, setListTrendingPrograms] = useState()
    const [numberOfFinishedProgram, setNumberOfFinishedProgram] = useState(0)
    const [numberOfProgressProgram, setNumberOfProgressProgram] = useState(0)
    const [numberOfUnLearnProgram, setNumberOfUnLearnProgram] = useState(0)
    const [totalPrograms, setTotalPrograms] = useState()
    // const [his, setHis] = useState()

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    useEffect(() => {
        getUserInfo()
    }, [])

    useEffect(() => {
        getMyProgram()
        getProgramsForInit()
    }, [userInfo])

    const getMyProgram = async () => {
        try {
            //program by role ==>
            const accessToken = localStorage.getItem('token')
            const res = await roleService.getRoleForUser(userInfo?.roleId?._id, accessToken)
            const data = res.data
            // console.log("program by role ==>", data)

            //program by user register and assigned ==>
            const resProgramRegist = await HistoryService.getList({ userId: userInfo?._id }, accessToken)
            // console.log("resProgramRegist", resProgramRegist);
            const dataProgramRegist = resProgramRegist.data.data.map((item) => {
                return {
                    ...item.programId,
                    processing: item.processing
                }
            })
            // console.log("dataProgramRegist ==>", dataProgramRegist)
            const result = dataProgramRegist
            data.data.freeProgramIds.forEach(element => {
                const exist = result.find((item) => item._id == element._id)
                if (!exist) {
                    result.push(element)
                }
            })
            data.data.paidProgramIds.forEach(element => {
                const exist = result.find((item) => item._id == element._id)
                if (!exist) {
                    result.push(element)
                }
            })
            // setListMyPrograms(dataProgramRegist.concat(data.freeProgramIds.map((item: any) => {
            //   return {
            //     ...item,
            //     // lessonIds: item.lessonIds.filter((lesson) => lesson.isDeleted != false)
            //   }
            // })).concat(data.paidProgramIds))
            // console.log("setListMyPrograms", result)
            setListMyPrograms(result.sort((item1, item2) => {
                return item1.processing - item2.processing
            }))
            getProgram(result)
        } catch (error) {
            console.error("error getMyProgram", error)
        }
    }

    const getProgram = async (notInList) => {
        try {
            const accessToken = localStorage.getItem('token')
            // console.log(accessToken);
            if (accessToken) {
                const res = await ProgramService.getList(accessToken)
                const data = res.data
                // console.log("data", data);
                const list = []
                data.data.forEach(element => {
                    const exist = notInList.find((item) => item._id == element._id)
                    if (!exist) {
                        list.push(element)
                    }
                })
                setListTrendingPrograms(list)
            }

        } catch (error) {
            console.error(error)
        }
    }

    const getProgramsForInit = async () => {
        try {
            const accessToken = localStorage.getItem('token')
            // console.log("params", params)
            const res = await HistoryService.getList({
                pageSize: 10000,
                userId: userInfo?._id
            }, accessToken)
            const list = res.data

            const listFinish = list.data.filter((item) => item.processing == 100)
            const listOnProgress = list.data.filter((item) => (item.processing && item.processing != 0 && item.processing != 100))
            console.log("list", list)

            setNumberOfFinishedProgram(listFinish?.length)
            setNumberOfProgressProgram(listOnProgress?.length)
            setNumberOfUnLearnProgram(res?.data.totalRecord - listFinish?.data?.length - listOnProgress?.data?.length)
            setTotalPrograms(res.data.totalRecord)
        } catch (error) {
            console.log("Error getListProgram", error)
        }

    }

    return (
        <div className="component">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Chương trình của tôi</div>
            </div>
            <div className="total">
                <div>
                    <TextField
                        placeholder="Tìm kiếm nội dung"
                        className="search"
                        size="small"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    style={{ display: showClearIcon }}
                                ></InputAdornment>
                            )
                        }}
                    />
                </div>
                <div className="group">
                    <div className="item">
                        <div className="num">{totalPrograms}</div>
                        <div className="text">Tổng số</div>
                    </div>
                    <div className="item">
                        <div className="num">{numberOfProgressProgram}</div>
                        <div className="text">Đang học</div>
                    </div>
                    <div className="item">
                        {numberOfUnLearnProgram ? <div className="num">{numberOfUnLearnProgram}</div> : <div className="empty">0</div>}
                        <div className="text">Chưa học</div>
                    </div>
                    <div className="item">
                        <div className="num">{numberOfFinishedProgram}</div>
                        <div className="text">Hoàn thành</div>
                    </div>
                </div>
            </div>
            {listMyPrograms?.map((x, key) => (
                <div className="list" key={key}>
                    <a href={`/program-detail?id=${x._id}`} className="about">
                        <div className="about-item">
                            <Image
                                className='img_list'
                                src={`${HOST}/${x?.photo}`}
                                width={50}
                                height={50}
                                alt="teenUp"
                            />
                            <div className="line-item">
                                <div className="line-1">{x.title}</div>
                                <div className="task-ellip">{x.description}</div>
                                <div className="task">{x.lessonIds.length} bài học</div>
                            </div>
                        </div>
                        <div className="loading">
                            {x?.processing ? <div className="percent">{x?.processing}%</div> : ""}
                        </div>
                    </a>
                </div>
            ))}
        </div>
    )
}
export default Page