'use client'
import React from 'react'
// import "./style.css"
import Protected from '../providers/protected'

export default function Page() {
    return (
        <Protected>
            <div className='container mx-auto'>
                {/* <div className='header'>
                    <img
                        className='avt'
                        src="/star.png"
                        width={50}
                        height={50}
                        alt="teenUp"
                    />
                    <div className='txt_head'>Admin</div>
                </div> */}
                <div className='detail'>
                    <a href='/info' className='option-detail'>Thông tin cá nhân</a>
                    <a href='/account-settings' className='option-detail'>Thiết lập tài khoản</a>
                    <a href='/application-information' className='option-detail'>Thông tin ứng dụng</a>
                    <a href='/login' className='option-detail'>Đăng xuất</a>
                </div>
            </div>
        </Protected>
    )
}
