'use client'
import React from 'react'
import FBChat from '../chat-fb/page'
import ZaloChat from '../chat-zalo/page'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import Image from 'next/image'
import { useRouter } from 'next/navigation';
import "./style.css"

function PageChat() {

    const router = useRouter()

    const handleClick = () => {
        router.push('/chat-header')
    }

    return (
        <>
            <div className="component-chat">
                <div className='live-chat-header'>
                    <div className='gr-live-chat'>
                        <ArrowBackIosIcon onClick={handleClick} />
                        <div className='title-chat-live'>Trợ lí học tập</div>
                    </div>
                </div>
                <div className='content'>
                    <Image
                        className='img'
                        src="/WE.png"
                        width={200}
                        height={200}
                        alt="teenUp"
                    />
                    <div className='text-head'>Xin chào! Xin chào! Xin xin chào</div>
                    <div className='text-size'>TeenUp là nơi thầy cô đã gặp rất nhiều bạn nhỏ tuyệt vời.
                        Cùng với sự tử tế và lòng dũng cảm, các bạn đã mang đến
                        đây và kể thầy cô nghe cực kì nhiều câu chuyện. Có những câu
                        chuyện kể về rắc rối như tơ vò cần giải đáp, cũng có khi chỉ
                        là những câu hỏi thăm, thắc mắc đơn giản thôi.. Dù là nội dung gì,
                        tất cả những mẩu chuyện đó đều trở thành những kỉ niệm cực kì quý giá
                        và các bạn học sinh trở thành những người đồng hành mà thầy cô sẽ
                        nhớ mãi về sau này.
                    </div>
                    <div className='text-size'>
                        Mỗi ngày, thầy cô đều rất háo hức để gặp những người bạn mới.
                        Các thầy cô TeenUp đang rất sẵn sàng để gặp bạn ngay bây giờ rồi.
                        Hãy chọn hình thức liên lạc thuận tiện nhất dưới đây và cùng
                        bắt đầu trao đổi với nhau mọi thứ trên đời nha!!
                    </div>
                </div>
                <FBChat />
                <ZaloChat />

            </div>

        </>
    )
}
export default PageChat