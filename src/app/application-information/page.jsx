'use client'
import React from 'react'
import {
    Link
} from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import "./style.css"

export default function Page() {
    return (
        <div className='container mx-auto'>
            <div className='title'>
                <Link className='logout' href="/information" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Thông tin ứng dụng</div>
            </div>
            <div className='app-header'>
                <div className='app-content'>Xin chào! Xin chào! Xin xin chào!</div>
                <div className='app-content-type'>TeenUp là gì? </div>
                <div className='text-head'>Nền tảng học và trải nghiệm trực tuyến các nội dung trong chương trình Giáo dục giới tính toàn diện</div>
                <ul className='detail'>
                    <div className='app-detail-item'>Các chương trình tập trung vào:</div>
                    <li className='detail-label'>• Xây dựng mối quan hệ (Healthy relationship building)</li>
                    <li className='detail-label'>• Kĩ năng tự vệ (Self-protection)</li>
                    <li className='detail-label'>• Phát triển bản ngã cá nhân (Self-identity)</li>
                    <li className='detail-label'>• Tự do lựa chọn trong cuộc sống (Life-preference)</li>
                    <li className='detail-label'>• Định hướng sự nghiệp (Career Lean-in)</li>
                </ul>
                <ul className='detail'>
                    <div className='app-detail-item'>Phương thức học được nghiên cứu kỹ lưỡng và triển khai theo các hình thức sau:</div>
                    <li className='detail-label'>• Học tập và thực hành hàng ngày</li>
                    <li className='detail-label'>• Hoàn thành thử thách theo từng chủ đề</li>
                    <li className='detail-label'>• Tham gia cuộc thi tranh tài giữa nhiều người học</li>
                    <li className='detail-label1'><div>•</div>&nbsp;Tiếp cận và trao đổi trực tiếp để giải đáp thắc mắc thông qua hệ thống chat sử dụng AI và chat với chuyên gia trong lĩnh vực</li>
                </ul>
                <div className='app-content-type'>Liên hệ với đội ngũ</div>
                <div className='text-head'>Bạn vui lòng lựa chọn một trong các cách thức liên hệ dưới đây. Đội ngũ TeenUp sẽ phản hồi bạn trong thời gian sớm nhất.</div>
                <div className='network'>
                    <img className='img-network' src="/mes.png" alt="" />
                    <div className='content-network'>Gửi tin nhắn Facebook Messenger</div>
                </div>
                <div className='network'>
                    <img className='img-network' src="/za.png" alt="" />
                    <div className='content-network'>Gửi tin nhắn Zalo</div>
                </div>
                <div className='network'>
                    <img className='img-network' src="/phone.png" alt="" />
                    <div className='content-network'>Liên hệ qua SĐT tổng đài: 0869 784 688</div>
                </div>
                <div className='network'>
                    <img className='img-network' src="/gmail.png" alt="" />
                    <div className='content-network'>Liên hệ qua email: info@wegrow.edu.vn </div>
                </div>
            </div>
        </div>
    )
}