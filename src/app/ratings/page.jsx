'use client'
import React, { useEffect, useState } from 'react'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import { getUser } from '@/networks/role.service'
import ScoreService from '../../networks/score.service';
import { HOST } from "@/networks/constants";
import "./style.css"

function Page() {

    const [userInfo, setUserInfo] = useState()
    const [score, setScore] = useState(0)
    const [position, setPosition] = useState([])
    const [listRank, setListRank] = useState([])

    useEffect(() => {
        getUserInfo()
    }, [])

    useEffect(() => {
        getScore()
        getRank()
    }, [userInfo])

    const getScore = async () => {
        console.log({ userInfo });
        if (!userInfo) return
        try {
            const accessToken = localStorage.getItem('token')
            const res = await ScoreService.getDetailByUserId(userInfo._id, accessToken)
            console.log("data getScore ---> ", res)

            const data = res.data
            if (data) {
                setScore(data.data.score)
                console.log("data.score", data.data.score);
            } else {
                // console.log("data getScore ---> ", data)
            }
        } catch (error) {
            console.error("error", error)
        }
    }

    const getRank = async () => {
        try {
            const accessToken = localStorage.getItem('token')
            const res = await ScoreService.getList({ pageSize: 10000 }, accessToken)
            const data = res.data
            // console.log("getRank", JSON.stringify(data))
            console.log("list rank", data);
            if (data) {
                const listUserForRender = data.data
                    .sort((a, b) => b.score - a.score)
                    .filter((item) => item.userId.roleId.ownerScore === true)
                setPosition(listUserForRender.findIndex((item) => item.userId?._id === userInfo?._id))
                setListRank(listUserForRender)
            }
            setListRank(data.data)
        } catch (error) {
            console.error('getRank', error)
        }
    }

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        if (user) {
            console.log(user);
            setUserInfo(user?.data?.data)
        }
    }

    return (
        <div className="component">
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Xếp hạng</div>
            </div>
            {/* {score?.map((x, y) => ( */}
            <div className="rating">
                <div className="level">
                    <div className="rank">Hạng</div>
                    <div className="rank">{position + 1}</div>
                </div>
                <div className="full-name">{userInfo?.name}</div>
                <div className="rank">{score} điểm</div>
            </div>
            {/* ))} */}
            <div className="table-view">
                <div className="content">Toàn bộ bảng xếp hạng</div>
                {listRank?.map((e, key) => (
                    <div className="table flex-1" key={key}>
                        <div className='flex-1'>{key + 1}</div>
                        <div className='flex-1'>
                            {
                                e?.userId?.photo ?
                                    <img
                                        className='img-ratings'
                                        alt="#"
                                        src={`${HOST}/${e.userId.photo}`}
                                    />
                                    :
                                    <img
                                        className='img-ratings'
                                        alt="#"
                                        src="./df.png"
                                    />
                            }
                        </div>

                        <div className='usename flex-1'>{e.userId.name}</div>
                        <div className='usename flex-1'>{e.score} điểm</div>
                    </div>
                ))}
            </div>
        </div>
    )
}
export default Page