'use client'
import React, { useState, useEffect } from 'react'
import dayjs from 'dayjs';
import {
    Link,
    FormControl,
    InputAdornment,
    TextField,
    createStyles,
    makeStyles,
    InputLabel,
    Select,
    MenuItem,
    Input
} from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { UploadOutlined } from '@ant-design/icons';
// import { UploadProps } from 'antd';
import { Button, message, Upload } from 'antd';
import { HOST } from '@/networks/constants';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { getUser } from '@/networks/role.service'
import toast from 'react-hot-toast';

import "./style.css"
import { useRouter } from 'next/navigation';
import axios from 'axios';

export default function Page() {
    const router = useRouter()
    const [token, setToken] = useState();
    const [sex, setSex] = useState('');
    // const [province, setProvince] = useState()
    const [open, setOpen] = useState(false);
    const [openSex, setOpenSex] = useState(false);
    const [userInfo, setUserInfo] = useState()
    const [name, setName] = useState()

    const handleChange = (event) => {
        console.log(event.target.value)
        setUserInfo({
            ...userInfo,
            gender: event.target.value
        })
    };

    const handleChange_province = (event) => {
        console.log(event.target.value)
        setUserInfo({
            ...userInfo,
            province: event.target.value
        })
    };

    const handleClose_sex = () => {
        setOpenSex(false);
    };

    const handleOpen_sex = () => {
        setOpenSex(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const Upload = () => {
        const formData = new FormData()

        // fetch(`${HOST}/user/${userInfo._id}`, {
        //     method: 'PATCH',
        //     body: formData,
        //     headers: {
        //         'Authorization': tokenStr
        //     }
        // })
    }
    useEffect(() => {
        setToken(localStorage.getItem('token'))
    }, [])

    useEffect(() => {
        getUserInfo()
    }, [])

    const UploadProps = {
        name: 'file',
        action: HOST,
        headers: {
            authorization: `Bearer ${token}`,
        }, onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        progress: {
            strokeColor: {
                '0%': '#108ee9',
                '100%': '#87d068',
            },
            strokeWidth: 3,
            format: (percent) => percent && `${parseFloat(percent.toFixed(2))}%`,
        },
    }

    const dataAddress = [
        {
            value: "An Giang",
            key: "An Giang"
        },
        {
            value: "Bà Rịa - Vũng Tàu",
            key: "Bà Rịa - Vũng Tàu"
        },
        {
            value: "Bắc Giang",
            key: "Bắc Giang"
        },
        {
            value: "Bắc Kạn",
            key: "Bắc Kạn"
        },
        {
            value: "Bạc Liêu",
            key: "Bạc Liêu"
        },
        {
            value: "Bắc Ninh",
            key: "Bắc Ninh"
        },
        {
            value: "Bến Tre",
            key: "Bến Tre"
        },
        {
            value: "Bình Định",
            key: "Bình Định"
        },
        {
            value: "Bình Dương",
            key: "Bình Dương"
        },
        {
            value: "Bình Phước",
            key: "Bình Phước"
        },
        {
            value: "Bình Thuận",
            key: "Bình Thuận"
        },
        {
            value: "Cà Mau",
            key: "Cà Mau"
        },
        {
            value: "Cần Thơ",
            key: "Cần Thơ"
        },
        {
            value: "Cao Bằng",
            key: "Cao Bằng"
        },
        {
            value: "Đà Nẵng",
            key: "Đà Nẵng"
        },
        {
            value: "Đắk Lắk",
            key: "Đắk Lắk"
        },
        {
            value: "Đắk Nông",
            key: "Đắk Nông"
        },
        {
            value: "Điện Biên",
            key: "Điện Biên"
        },
        {
            value: "Đồng Nai",
            key: "Đồng Nai"
        },
        {
            value: "Đồng Tháp",
            key: "Đồng Tháp"
        },
        {
            value: "Gia Lai",
            key: "Gia Lai"
        },
        {
            value: "Hà Giang",
            key: "Hà Giang"
        },
        {
            value: "Hà Nam",
            key: "Hà Nam"
        },
        {
            value: "Hà Nội",
            key: "Hà Nội"
        },
        {
            value: "Hà Tĩnh",
            key: "Hà Tĩnh"
        },
        {
            value: "Hải Dương",
            key: "Hải Dương"
        },
        {
            value: "Hải Phòng",
            key: "Hải Phòng"
        },
        {
            value: "Hậu Giang",
            key: "Hậu Giang"
        },
        {
            value: "Hòa Bình",
            key: "Hòa Bình"
        },
        {
            value: "Hưng Yên",
            key: "Hưng Yên"
        },
        {
            value: "Khánh Hòa",
            key: "Khánh Hòa"
        },
        {
            value: "Kiên Giang",
            key: "Kiên Giang"
        },
        {
            value: "Kon Tum",
            key: "Kon Tum"
        },
        {
            value: "Lai Châu",
            key: "Lai Châu"
        },
        {
            value: "Lâm Đồng",
            key: "Lâm Đồng"
        },
        {
            value: "Lạng Sơn",
            key: "Lạng Sơn"
        },
        {
            value: "Lào Cai",
            key: "Lào Cai"
        },
        {
            value: "Long An",
            key: "Long An"
        },
        {
            value: "Nam Định",
            key: "Nam Định"
        },
        {
            value: "Nghệ An",
            key: "Nghệ An"
        },
        {
            value: "Ninh Bình",
            key: "Ninh Bình"
        },
        {
            value: "Ninh Thuận",
            key: "Ninh Thuận"
        },
        {
            value: "Phú Thọ",
            key: "Phú Thọ"
        },
        {
            value: "Phú Yên",
            key: "Phú Yên"
        },
        {
            value: "Quảng Bình",
            key: "Quảng Bình"
        },
        {
            value: "Quảng Nam",
            key: "Quảng Nam"
        },
        {
            value: "Quảng Ngãi",
            key: "Quảng Ngãi"
        },
        {
            value: "Quảng Ninh",
            key: "Quảng Ninh"
        },
        {
            value: "Quảng Trị",
            key: "Quảng Trị"
        },
        {
            value: "Sóc Trăng",
            key: "Sóc Trăng"
        },
        {
            value: "Sơn La",
            key: "Sơn La"
        },
        {
            value: "Tây Ninh",
            key: "Tây Ninh"
        },
        {
            value: "Thái Bình",
            key: "Thái Bình"
        },
        {
            value: "Thái Nguyên",
            key: "Thái Nguyên"
        },
        {
            value: "Thanh Hóa",
            key: "Thanh Hóa"
        },
        {
            value: "Thừa Thiên Huế",
            key: "Thừa Thiên Huế"
        },
        {
            value: "Tiền Giang",
            key: "Tiền Giang"
        },
        {
            value: "Thành phố Hồ Chí Minh",
            key: "Thành phố Hồ Chí Minh"
        },
        {
            value: "Trà Vinh",
            key: "Trà Vinh"
        },
        {
            value: "Tuyên Quang",
            key: "Tuyên Quang"
        },
        {
            value: "Vĩnh Long",
            key: "Vĩnh Long"
        },
        {
            value: "Vĩnh Phúc",
            key: "Vĩnh Phúc"
        },
        {
            value: "Yên Bái",
            key: "Yên Bái"
        }
    ]

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            console.log("user", user);
        }
    }

    const saveInfomation = async () => {
        try {
            console.log('userInfo', userInfo);

            const formData = new FormData()
            formData.append("name", userInfo.name.trim())
            formData.append("phone", userInfo.phone)
            formData.append("email", userInfo.email)
            formData.append("province", userInfo.province)
            formData.append("address", userInfo.address)
            // if (photo) {
            //     formData.append('file', {
            //         name: photo.fileName,
            //         type: photo.type,
            //         uri: Platform.OS === 'ios' ? photo.uri.replace('file://', '') : photo.uri,
            //     })
            // }
            if (userInfo.dob) {
                formData.append("dob", userInfo.dob)
            }

            if (userInfo.gender) {
                formData.append("gender", userInfo.gender)
            }
            if (userInfo.schoolName) {
                formData.append("schoolName", userInfo.schoolName)
            }
            if (userInfo.refFatherName) {
                formData.append("refFatherName", userInfo.refFatherName)
            }
            if (userInfo.refMotherName) {
                formData.append("refMotherName", userInfo.refMotherName)
            }
            if (userInfo.refFatherPhone) {
                formData.append("refFatherPhone", userInfo.refFatherPhone)
            }
            if (userInfo.refMotherPhone) {
                formData.append("refMotherPhone", userInfo.refMotherPhone)
            }
            console.log("formData", formData)
            // setLoading(true)

            const token = localStorage.getItem("token")
            const user = await getUser(token)
            console.log('user', user);



            axios.patch(`${HOST}/user/${userInfo._id}`, formData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
                // .then((response) => response.json())
                .then(async (response) => {
                    console.log('response', response)
                    //  await getUserInfo()
                    toast.success("Cập nhật thông tin thành công, hãy đăng nhập lại!")
                    localStorage.removeItem('token')
                    router.push('/login')
                    // setLoading(false)
                    // setTimeout(() => {
                    //     console.log("COMBANK TO PROFILE PAGE =========>")
                    //     navigation.navigate({
                    //         name: 'ProfilePage',
                    //         params: {
                    //             refresh: true
                    //         },
                    //         merge: true
                    //     })
                    // }, 300)
                })
                .catch((error) => {
                    // setLoading(false)
                    console.log('error', error)
                })
        } catch (error) {
            console.error("saveInfomation", error)
            Alert.alert("Đã có lỗi xảy ra vui lòng thử lại sau!")
        }

    }

    return (
        <div className='container mx-auto'>
            <div className='title'>
                <Link className='logout' href="/home" underline="always">
                    <ArrowBackIosIcon />
                </Link>
                <div className='txt_head'>Thông tin cá nhân</div>
            </div>
            <div className='detail'>
                <img
                    className='avt'
                    src="/star.png"
                    width={70}
                    height={70}
                    alt="teenUp"
                />
                {/* <Upload {...UploadProps} className='upload'>
                    <Button icon={<UploadOutlined />}>Click to Upload</Button>
                </Upload> */}
                <a className='upload'>Thay đổi ảnh đại diện</a>
                <Input onChange={(event) => {
                    setUserInfo({
                        ...userInfo,
                        name: event.target.value,
                    })
                }} className='custom_input'
                    id="outlined-read-only-input"
                    placeholder="Họ và tên"
                    value={userInfo?.name} />
                <Input
                    className='custom_input'
                    id="standard-number"
                    label="Số điện thoại"
                    type="number"
                    variant="standard"
                    onChange={(event) => {
                        setUserInfo({
                            ...userInfo,
                            phone: event.target.value,
                        })
                    }}
                    value={userInfo?.phone}
                />
                <Input onChange={(event) => {
                    setUserInfo({
                        ...userInfo,
                        email: event.target.value,
                    })
                }} className='custom_input' id="standard-basic" variant="standard" label="Email" value={userInfo?.email} />
                <div className='gr'>
                    {/* <InputLabel id="demo-simple-select-label">Giới tính</InputLabel> */}
                    {userInfo && (<FormControl sx={{ m: 1, minWidth: 120 }}>
                        <InputLabel id="demo-controlled-open-select-label">Giới tính</InputLabel>
                        <Select
                            labelId="demo-controlled-open-select-label"
                            id="demo-controlled-open-select"
                            open={openSex}
                            onClose={handleClose_sex}
                            onOpen={handleOpen_sex}
                            label="sex"
                            onChange={handleChange}
                            value={userInfo?.gender}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Nam"}>Nam</MenuItem>
                            <MenuItem value={"Nữ"}>Nữ</MenuItem>
                            <MenuItem value={"Khác"}>Khác</MenuItem>
                        </Select>
                    </FormControl>)}
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker format='DD/MM/YYYY' label="Ngày sinh" onChange={(value) => {
                            // console.log(`${value.date()}/${value.month() + 1}/${value.year()}`);
                            setUserInfo({
                                ...userInfo,
                                dob: `${value.date()}/${value.month() + 1}/${value.year()}`,
                            })
                        }}
                            value={(dayjs(userInfo?.dob, "DD/MM/YYYY"))}
                        />
                    </LocalizationProvider>
                </div>
                <div className='gr'>
                    {userInfo && (<FormControl sx={{ m: 1, minWidth: 120 }}>
                        <InputLabel id="demo-controlled-open-select-label">Tỉnh thành</InputLabel>
                        <Select
                            labelId="demo-controlled-open-select-label"
                            id="demo-controlled-open-select"
                            open={open}
                            onClose={handleClose}
                            onOpen={handleOpen}
                            value={userInfo?.province}
                            label="province"
                            onChange={handleChange_province}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {dataAddress?.map((x, key) => (
                                <MenuItem key={key} value={x.value}>{x.value}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>)}
                    <Input onChange={(event) => {
                        setUserInfo({
                            ...userInfo,
                            address: event.target.value,
                        })
                    }} className='address' id="standard-basic" variant="standard" placeholder="Địa chỉ" value={userInfo?.address} />
                </div>
                <div className='item'>
                    {userInfo && (<Input onChange={(event) => {
                        setUserInfo({
                            ...userInfo,
                            schoolName: event.target.value,
                        })
                    }} className='school' id="standard-basic" variant="standard" placeholder="Trường học" value={userInfo?.childIds?.schoolName} />)}
                </div>
                <div className='info'>
                    <div className='label'>Thông tin phụ huynh</div>
                    <div className='info-father'>
                        <Input onChange={(event) => {
                            setUserInfo({
                                ...userInfo,
                                refFatherName: event.target.value,
                            })
                        }} className='info-item' id="standard-basic" variant="standard" placeholder="Họ tên bố" value={userInfo?.childIds?.refFatherName} />
                        <Input
                            className='num-item'
                            id="standard-number"
                            placeholder="Số điện thoại bố"
                            type="number"
                            variant="standard"
                            onChange={(event) => {
                                setUserInfo({
                                    ...userInfo,
                                    refFatherPhone: event.target.value,
                                })
                            }}
                        />
                    </div>
                    <div className='info-mother'>
                        <Input onChange={(event) => {
                            setUserInfo({
                                ...userInfo,
                                refMotherName: event.target.value,
                            })
                        }} className='info-item' id="standard-basic" variant="standard" placeholder="Họ tên mẹ" value={userInfo?.childIds?.refMotherName} />
                        <Input
                            onChange={(event) => {
                                setUserInfo({
                                    ...userInfo,
                                    refMotherPhone: event.target.value,
                                })
                            }}
                            className='num-item'
                            id="standard-number"
                            placeholder="Số điện thoại mẹ"
                            type="number"
                            variant="standard"
                            value={userInfo?.childIds?.refMotherPhone}
                        />
                    </div>
                </div>
                <button className='save' onClick={saveInfomation}>Lưu thông tin</button>
            </div>
        </div>
    )
}
