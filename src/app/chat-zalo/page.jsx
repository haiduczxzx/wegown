"use client"
import React from 'react'
import Script from 'next/script'
import "./style.css"

export default function page() {
    return (
        <div>
            <div className="zalo-chat-widget" data-oaid="245491851162010758" data-welcome-message="Rất vui khi được hỗ trợ bạn!" data-autopopup="0" data-width="" data-height=""></div>
            <Script src="https://sp.zalo.me/plugins/sdk.js"></Script>
        </div>
    )
}