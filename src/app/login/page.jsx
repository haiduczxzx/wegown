'use client'
import "./style.css"
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation'
import Image from 'next/image'
import { TextField, Link, FormControl, InputLabel, Input, InputAdornment, IconButton } from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Button } from 'antd';
import axios from 'axios';
import toast from 'react-hot-toast';
import { getUser, login } from '@/networks/role.service'
import { useDispatch, useSelector } from "react-redux";
import { setAuthUserInfo } from "@/redux/auth/authSlice";
import './style.css'




function Page() {
  const dispatch = useDispatch()
  const router = useRouter()
  // Nút này xử lí phần password
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [passwordValidate, setPasswordValidate] = useState('')
  const [usernameValidate, setUsernameValidate] = useState()
  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const validate = () => {
    if (passwordValidate || usernameValidate) {
      return false;
    }
    return true
  }

  const renderValidateString = () => {
    if (password.length < 5) {
      setPasswordValidate("Mật khẩu không được ít hơn 5 kí tự")
    } else {
      setPasswordValidate(null)
    }

    if (username.length <= 0) {
      setUsernameValidate("Tên tài khoản không được để trống")
    } else {
      setUsernameValidate(null)
    }
  }

  const [data, setData] = React.useState(null);
  //const api = `https://be-wegown.bongngotv.vip`;

  const onChangeUsername = (e) => {
    setUsername(e.target.value)
  }

  const onChangePassword = (e) => {
    setPassword(e.target.value)
  }

  const handleClick = async () => {
    renderValidateString()
    console.log(validate());
    if (validate()) {
      console.log({ username, password });
      try {
        const response = await login({ username, password })
        console.log("response", { ...response });
        if (response.data.code === 200) {
          localStorage.setItem('token', response.data.data.access_token)
          var responseUserData = await getUser(response.data.data.access_token);
          console.log('redux user', responseUserData)
          dispatch(setAuthUserInfo(responseUserData.data.data))
          //var userRedux = useSelector(state => state.user)
          // console.log('user Redux', userRedux)
        }
        setData('data', response.data);

        toast.success("Đăng nhập thành công")
        router.push('/home')


      } catch (error) {
        toast.error(error)
      }
    }


  };

  return (
    <div className='component-login'>
      <Image
        className='img-login'
        src="/img.jpg"
        width={250}
        height={73}
        alt="teenUp"
      />
      <div className='form-login-app'>
        <form>
          <TextField onChange={onChangeUsername} error={!!usernameValidate} className='custom_input-app' id="standard-basic" variant="standard" placeholder='Tên đăng nhập / Email / SĐT' />
          {usernameValidate && (<div className="text-red-700">{usernameValidate}</div>)}
          <FormControl className='custom_input-app' variant="standard" placeholder="Please enter text">
            {/* <InputLabel htmlFor="standard-adornment-password">Mật khẩu</InputLabel> */}
            <Input
              className='custom_input-password'
              onChange={onChangePassword}
              error={!!passwordValidate}
              placeholder='Mật khẩu'
              id="standard-adornment-password"
              type={showPassword ? 'text' : 'password'}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
            {passwordValidate && (<div className="text-red-700">{passwordValidate}</div>)}
          </FormControl>
          <Button onClick={handleClick} className='custom-button-app' block>Đăng nhập</Button>
          <div className='register-app'>
            <p>Bạn chưa có tài khoản?</p>
            <Link className='link_register-app' href="/register" underline="always">
              {'Đăng kí ngay'}
            </Link>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Page
