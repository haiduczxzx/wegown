"use client"
import React, { useEffect, useState } from 'react'
import Protected from '../providers/protected';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { Link } from '@mui/material';
import { useSearchParams } from "next/navigation"
import axios from 'axios';
import "./style.css"
import { HOST } from '@/networks/constants';
import { useRouter } from 'next/navigation';
import HistoryTestService from '../../networks/history-test.service'
import { getUser } from '@/networks/role.service'
import toast from 'react-hot-toast';



export default function Page() {
    const [userInfo, setUserInfo] = useState()
    const [lesson, setLesson] = useState()
    const searchParams = useSearchParams()
    const id = searchParams.get('id')
    const historyId = searchParams.get('historyId')
    const router = useRouter()

    // console.log(id);
    const isValidUrl = urlString => {
        var urlPattern = new RegExp('^(https?:\\/\\/)?' + // validate protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // validate domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // validate OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // validate port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // validate query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // validate fragment locator
        return !!urlPattern.test(urlString);
    }

    useEffect(() => {
        var token = localStorage.getItem("token")
        getLesson(token).then((v) => {
            console.log('lesson', v);
            setLesson(v?.data?.data)
        })

    }, [])

    useEffect(() => {
        console.log('historyId', historyId);
        getUserInfo()
    }, [])


    const getLesson = async (accessToken) => {
        return await axios.get(`${HOST}/lesson/${id}`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        })
    }

    const getUserInfo = async () => {
        const token = localStorage.getItem("token")
        const user = await getUser(token)
        // console.log(user);
        if (user) {
            setUserInfo(user?.data?.data)
            // console.log("user", user);
        }
    }

    const onClickFinish = async () => {
        var token = localStorage.getItem("token")
        getUserInfo()
        try {
            const resHistoryTest = await HistoryTestService.create({
                "userId": userInfo._id,
                "historyId": historyId,
                // "exerciseId": "64ca2d080ed42f3fc2ca489d",
                "lessonId": lesson._id,
                "score": 0,
                "processing": 100
            }, token)
            router.push('/home')
            toast.success("Bạn đã hoàn thành bài học")
        } catch (error) {
            toast.error(`Lỗi! ${error}`)
        }

    }

    return (
        <Protected>
            <div className='container mx-auto'>
                <div className='title'>
                    <button className='logout' href="/home" underline="always" onClick={() => router.back()} >
                        <ArrowBackIosIcon />
                    </button>
                    <div className='txt_head'>Chi tiết bài học</div>
                </div>
                <div>
                    <div className='pb-4 pt-4'>{lesson?.description}</div>
                    <div className='pb-4'>Thực hiện ngày thử thách dưới đây để hoàn thành bài học và nhận điểm tích lũy dưới đây nhé!</div>
                </div>

                {isValidUrl(lesson?.exerciseIds[0]?.linkQuiz)
                    ?
                    <button className='btn' onClick={() => router.push(`/perform?id=${lesson._id}`)}>Thực hiện thử thách</button>
                    :
                    <button className='btn' onClick={onClickFinish}>Hoàn thành bài học</button>}
            </div>
        </Protected>
    )
}
