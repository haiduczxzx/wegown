import { create } from "domain"
import { Role } from "../interface"
import { createSlice } from "@reduxjs/toolkit"
import { current } from '@reduxjs/toolkit'

export interface UserState {
  _id?: string,
  accessToken?: string,
  dob?: string,
  username?: string,
  name?: string,
  password?: string,
  email?: string,
  province?: string,
  address?: string,
  childIds?: any[],
  code?: string,
  seq?: number,
  status?: number,
  createdAt?: string,
  updatedAt?: string,
  photo?: string,
  tokenFcm?: string,
  gender?: string,
  schoolName?: string
  roleId?: Role
}

var initialState: UserState = {
}

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setAuthUserInfo: (_, action) => {
      return action.payload
    }
  }
});
export const { setAuthUserInfo } = userSlice.actions;
export default userSlice.reducer;
