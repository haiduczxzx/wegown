'use client'
import { Provider, useDispatch } from 'react-redux'
import React, { useEffect } from "react";
import { store } from './store';


export default function StoreProvider({ children }) {
  return (<Provider store={store}>
    {children}
  </Provider>)
}
